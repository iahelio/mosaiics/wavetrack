
def normalize_list(lst, a, b):
    flat_lst = [item for sublist in lst for item in sublist]  # Flatten the 2D list
    min_val = min(flat_lst)
    max_val = max(flat_lst)
    normalized_lst = [[(item - min_val) / (max_val - min_val) for item in sublist] for sublist in lst]
    scaled_lst = [[item * (b - a) + a for item in sublist] for sublist in normalized_lst]
    return scaled_lst



def normalize_3d_list(lst, a, b):
    flat_lst = [item for sublist in lst for subsublist in sublist for item in subsublist]  # Flatten the 3D list
    min_val = min(flat_lst)
    max_val = max(flat_lst)
    normalized_lst = [[[((item - min_val) / (max_val - min_val)) for item in subsublist] for subsublist in sublist] for sublist in lst]
    scaled_lst = [[[item * (b - a) + a for item in subsublist] for subsublist in sublist] for sublist in normalized_lst]
    return scaled_lst


def get_min_max_3d_list(lst):
    min_val = float('inf')
    max_val = float('-inf')

    for sublist in lst:
        for subsublist in sublist:
            for item in subsublist:
                if item < min_val:
                    min_val = item
                if item > max_val:
                    max_val = item

    return min_val, max_val
