import t_base_img
import t_math
import numpy as np
from scipy import misc
from scipy import ndimage
from scipy.stats import norm
import matplotlib.pyplot as plt
from matplotlib import cm
from PIL import Image, ImageOps, ImageFilter
from scipy.optimize import curve_fit
import os
import gc
from t_config import t_decompose_config


class t_atrous_wavelet(t_base_img.t_base_img):
    # Inherits t_base_img. Represents basic wavelet decomposition routines
    # Works as a parent for t_mask class. Some parts are ported from iAthena7 GeoWizard
    
    def calc_ij(self, i0, j):
         # calculate position which i0 element of the initial kernel matrix 
         # would have in j-itteration (scale) matrix
         ij = (i0-1)* ( ( (2**(j-1))-1 )) + i0 
         return ij


    def calc_kernel_j(self, kernel_init, j):
        
        col_i_0 = 0; 
        col_n_0 = len(self.kernel_init[0]); # number of columns in the initial kernel 
        row_n_0 = len(self.kernel_init); # number of rows in the initial kernel 

        col_n_j = self.calc_ij(col_n_0,j);  # number of columns in iteration j of the kernel 
        row_n_j = self.calc_ij(row_n_0,j); # number of rows in iteration j of the kernel

        # Instead of modifying kenel at the each step by shifting rows and columns we
        # pre-initialize j-iterration of the kernel, calculate non-zero elements coordinates and fill values
        # from the initial kernel 
         
        kernel_j = [[0 for col_i in range(col_n_j)] for row_i in range(row_n_j)];
       
        # iterate through the columns of initial kernel
        for col_i_0 in range(col_n_0):  
        # iterate through the rows of initial kernel 
            for row_i_0 in range(row_n_0):
                #obtaining col_i_j and row_i_j      
                col_i_j = self.calc_ij(col_i_0,j);
                row_i_j = self.calc_ij(row_i_0,j);
                kernel_j[row_i_j][col_i_j] = self.kernel_init[col_i_0][row_i_0];
        
        return kernel_j


    def calc_scales(self):
        
        # itteratiove convolution calculation
        
        m_math=t_math.t_math()
        
        print('A-trou wavelet transform...\n')

        self.w_coeffs.append(self.img_init_data);
        #fitted_mean,fitted_std=norm.fit(np.array(self.w_coeffs[0]).astype='int32') 
        fitted_mean, fitted_std = norm.fit(np.array(self.w_coeffs[0]).astype('int32'))

        self.w_coeffs_std.append(fitted_std)
        

        for j in range(1,self.decompose_config.scales_n_max):  

            # As an alternative: 
            # sciy.ndimage.filters.convolve(init_img, weights output=None, mode='reflect', cval=0.0, origin=0)
            # self.w_coeffs.append(ndimage.convolve(self.w_coeffs[j-1], self.kernel_j[j], mode='reflect')); 
            # signal.convolve2d(in1, in2, mode='full', boundary='fill', fillvalue=0)[source]
            kernel_j=self.calc_kernel_j(self.kernel_init,j)
        
            print('Convolving scale ' +str(j)+ ' with ' + str(len(kernel_j))+'x'+ str(len(kernel_j[0]))+' kernel')    
            w_coeff_j_arr= m_math.convolve_list_arr(self.w_coeffs[j-1], kernel_j) 
            

            if (self.decompose_config.apply_thrhold_circle_mask =='n'):  
                
                fitted_mean,fitted_std=norm.fit(w_coeff_j_arr)  
                self.w_coeffs_std.append(fitted_std)
                print('Truncating with standard deviation:'+str(fitted_std))   
                thrhold_abs= self.decompose_config.thrhold_sigma*fitted_std 
                w_coeff_j_arr_trunc = (w_coeff_j_arr>thrhold_abs)*w_coeff_j_arr  
            
            else: 
                
                w_coeff_j_arr_inside = w_coeff_j_arr * ( np.array(self.circle_mask) > 0)
                w_coeff_j_arr_outside = w_coeff_j_arr * ( np.array(self.circle_mask) == 0)
                
                fitted_mean_inside,fitted_std_inside=norm.fit(w_coeff_j_arr_inside) 
                fitted_mean_outside,fitted_std_outside=norm.fit(w_coeff_j_arr_outside)
                
                thrhold_abs_inside= self.decompose_config.thrhold_sigma*fitted_std_inside
                w_coeff_j_arr_trunc_inside = (w_coeff_j_arr_inside>thrhold_abs_inside)*w_coeff_j_arr_inside  
                
                thrhold_abs_outside= self.decompose_config.outside_thrhold_sigma*fitted_std_outside
                                                           
                w_coeff_j_arr_trunc_outside = (w_coeff_j_arr_outside>thrhold_abs_outside)*w_coeff_j_arr_outside
                w_coeff_j_arr_trunc = np.add( w_coeff_j_arr_trunc_inside, w_coeff_j_arr_trunc_outside)


            self.w_coeffs.append(list(w_coeff_j_arr_trunc)) 
        

        print('Calculating wavelet scales...\n')
        self.scales.append(self.img_init_data)  

        for j in range(1,self.decompose_config.scales_n_max): 
            w_coeff_jm1_arr=np.array(self.w_coeffs[j-1],dtype='int32')
            w_coeff_j_arr=np.array(self.w_coeffs[j],dtype='int32')
            w_coeffs_sub=np.subtract(w_coeff_jm1_arr,w_coeff_j_arr)
            self.scales.append(list(w_coeffs_sub))
    
        print('Restoring image from filtered scale with weights...')    
        restored_img_arr=np.array(self.w_coeffs[self.decompose_config.scales_n_max-1],dtype='int32')

        for j in range(1,self.decompose_config.scales_n_max):   
            scales_j_arr=np.array(self.scales[j],dtype='int32')
            scales_j_weights_arr=np.multiply(scales_j_arr, self.decompose_config.scales_weights[j])
            restored_img_arr=np.add(restored_img_arr,scales_j_weights_arr)

        self.img_restored=list(restored_img_arr)
        self.w_coeffs.append(list(restored_img_arr)) 
        self.scales.append(list(restored_img_arr)) 
        self.decompose_config.scales_n_max=self.decompose_config.scales_n_max+1

        print('Applying reference mask to the reconstructed image...')
        
        reference_arr = np.array(self.w_coeffs[self.decompose_config.reference_scale_n])
        reference_mask_arr = (reference_arr>0)
        self.reference_mask = list(reference_mask_arr)
        self.w_coeffs.append(list( np.multiply(self.img_restored, reference_mask_arr)))
        self.scales.append(list( np.multiply(self.img_restored, reference_mask_arr)))
        self.decompose_config.scales_n_max=self.decompose_config.scales_n_max+1


    def save_scales(self,path):
       
        a_math = t_math.t_math()

        for j in range(0,self.decompose_config.scales_n_max):
           
            print('Saving scale:'+str(j))
            
            if (self.decompose_config.save_scales=='y'):
               a_math.save_data_pic(self.scales[j][:],path+'/out_'+'t_'+str(self.timestep_i)+'_'+self.datetime_str+'_wlgth_'+str(self.wavelength)+'_scale_'+str(j)+'.png',self.preproc_config.x_size,self.preproc_config.y_size)
            
            a_math.save_data_pic(self.w_coeffs[j][:], path + '/out_'+'t_'+str(self.timestep_i)+'_'+self.datetime_str+'_wlgth_'+str(self.wavelength)+'_wcoeff_'+str(j)+'.png',self.preproc_config.x_size,self.preproc_config.y_size)
              
        print('Saving reconstructed image with wavelet scales filtered separately')
        a_math.save_data_pic(self.img_restored, path+'/out_'+'t_'+str(self.timestep_i)+'_'+ self.datetime_str+'_wlgth_'+str(self.wavelength)+'_restored_w_trhold.png',self.preproc_config.x_size,self.preproc_config.y_size)
       
              
    def read_single_scale(self,fname):

        math = t_math.t_math()
        print('Reading single scale filename: '+fname)
        data_rgb=plt.imread(fname)
        list_grayscale=list(math.rgb_2_gray(data_rgb))
        datatime_str=[4,26]
        wavelength_str=[34,36]
        scale_str= [44]
        print('Date-time: '+datetime_str)
        print('Wavelength: '+wavelength_str)
        print('Wavelet scale:'+scale_str)

        file_datetime_obj=datetime.datetime.strptime(datetime_str,'%Y-%m-%dT%H:%M:%S.%f' )
        timestamp=file_datetime_obj.timestamp()  

        return list_grayscale,datetime_str,wavelength,timestamp
    

    def __init__(self): 

        t_base_img.t_base_img.__init__(self)
    
        self.scales = []  # scale         
        self.img_restored = []  # restored image for control purposes
        self.w_coeffs = []      #  wavelet coefficients. Convolution product of a previous element of the array and j-"version" of the kernel
        self.w_coeffs_std = []  # Standard deviations of wavelet coefficients 

        self.kernel_init = [[1/256, 1/64, 3/128, 1/64, 1/256],
                           [1/64, 1/16, 3/32, 1/16, 1/64],
                           [3/128, 3/32, 9/64, 3/32, 3/128],
                           [1/64, 1/16, 3/32, 1/16, 1/64],
                           [1/256, 1/64, 3/128, 1/64, 1/256]] # A-Trous Kernel
        

        self.decompositions = [] # could be either wavelet coefficients (convolutions in t_atrous_wavelet) or scales with different post-processing
        self.decompositions_raw = []
        self.decompositions_std = []
        self.reference_mask = []
