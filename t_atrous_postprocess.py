from PIL import Image, ImageOps, ImageFilter
from scipy.optimize import curve_fit
import t_atrous_wavelet
import numpy as np
from scipy.stats import norm
import t_math


class t_atrous_postprocess(t_atrous_wavelet.t_atrous_wavelet): 


    def scales_post_process(self):
        
     
        print('Post processing wavelet decomposition results...')
     
        for j in range(0,self.decompose_config.scales_n_max):
            print('Scale:'+str(j))
            int32ii=np.iinfo(np.int32)
           
            if (self.decompose_config.use_scales_not_coeff=='y'): 
                print('Moving ahead with wavelet scales, NOT coefficients...')
                decomposition_j_raw_arr = np.array(self.scales[j],dtype='int32')
                decomposition_j_raw_max=np.amax(self.scales[j])
                decomposition_j_raw_min = np.amin(self.scales[j])
                self.decompositions_raw.append(list(decomposition_j_raw_arr))

            else:
                print('Moving ahead with wavelet coefficients, NOT scales...')
                decomposition_j_raw_arr = np.array(self.w_coeffs[j],dtype='int32')
                decomposition_j_raw_max=np.amax(self.w_coeffs[j])
                decomposition_j_raw_min = np.amin(self.w_coeffs[j])
                self.decompositions_raw.append(list(decomposition_j_raw_arr))

          
            if (self.decompose_config.normalize=='y'): 
                print('Normalizing data, bringing to '+str(self.decompose_config.resolution_bits)+' bits resolution...')
                decomposition_j_ratio= np.amax(decomposition_j_raw_max)/pow(2,self.decompose_config.resolution_bits)
                decomposition_j_arr_plus= np.subtract(decomposition_j_raw_arr,decomposition_j_raw_min)
                decomposition_j_arr_div_f = np.divide(decomposition_j_arr_plus, decomposition_j_ratio)
                decomposition_j_arr_div_f_r = np.round(decomposition_j_arr_div_f)
                print('Normalizing/bringing the data to [0...'+ str(int32ii.max)+'] scale...'  )
                decomposition_j_arr_norm_f = np.round(np.multiply(decomposition_j_arr_div_f_r.astype(np.int32),int32ii.max/pow(2,self.decompose_config.resolution_bits)))
                decomposition_j_arr_norm = decomposition_j_arr_norm_f.astype(np.int32)
      
            else: 
                print('Skipping data normalization and scale adjustment, gowing forward with RAW...')
                decomposition_j_arr_norm = np.array(decomposition_j_raw_arr) 
            
            #add invert scales

            #use poserized scales for feliment with coeff/3 and bitrate 4 

            if (self.decompose_config.PIL_posterize=='y'): 
                print('Pillow posterize with value = '+str(self.decompose_config.PIL_posterize_ratio) +' ...')
                decomposition_j_img_norm=Image.fromarray(decomposition_j_arr_norm,'I')
                decomposition_j_img_norm=decomposition_j_img_norm.convert('RGB')
                decomposition_j_img_norm_posterize = ImageOps.posterize(decomposition_j_img_norm,self.decompose_config.PIL_posterize_ratio)
                self.decompositions.append(list(decomposition_j_img_norm_posterize))
               
            elif (self.decompose_config.median_filt=='y'):
            
                print('Median filtering with scale parameter = '+str(self.decompose_config.median_scale)+'...')
                decomposition_j_img_norm=Image.fromarray(decomposition_j_arr_norm,'I')
                decomposition_j_img_norm=decomposition_j_img_norm.convert('RGB')        
                decomposition_j_img_norm_median = decomposition_j_img_norm.filter(ImageFilter.MedianFilter(self.decompose_config.median_scale))
                self.decompositions.append(list(decomposition_j_img_norm_median))

            else: 
                print('Using normalized data as decomposition for scale'+str(j))
                self.decompositions.append(list(decomposition_j_arr_norm))
            
               

        print('Calculating standard deviations for decompositions...')
        for scale_i in range(0,self.decompose_config.scales_n_max): 
            print('Scale: '+str(scale_i))
            decomposition_j_arr= np.array(self.decompositions[scale_i])
            fitted_mean,fitted_std=norm.fit(decomposition_j_arr)   
            self.decompositions_std.append(fitted_std)
               
 
    def save_decompositions(self,path):
       
        a_math = t_math.t_math()

        for j in range(0,self.decompose_config.scales_n_max):
           
            print('Saving post processed and RAW wavelet decompositions:'+str(j))           
            a_math.save_data_pic(self.decompositions[j][:],path+'/out_'+'t_'+str(self.timestep_i)+'_'+self.datetime_str+'_wlgth_'+str(self.wavelength)+'_decomp_'+str(j)+'.png',self.preproc_config.x_size,self.preproc_config.y_size)
            a_math.save_data_pic(self.decompositions_raw[j][:], path + '/out_'+'t_'+str(self.timestep_i)+'_'+self.datetime_str+'_wlgth_'+str(self.wavelength)+'_decomp_raw_'+str(j)+'.png',self.preproc_config.x_size,self.preproc_config.y_size)
            


def __init__(self): 
 
        t_atrous_wavelet.t_atrous_wavelet.__init__(self)
        

       