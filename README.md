
Wavetrack v. 0.41. The software for Multi-Scale, Multi-Instrument Image Processing and Image Segmentation in Heliophysics. 

The algorithmic tracking of an arbitrary time-evolving object with only general preliminary guesses about its shape-intensity characteristics could be a challenging task due to a variety of uncertainties. Coronal Mass-Ejection (CME) Shock Waves are usually very dim objects, which exist in a very narrow part of the extremely wide dynamic range of the RAW image data. This fact generally makes these objects even harder to recognize and track with known techniques, particularly on relatively noisy EUV or radio images. Nevertheless, it is still achievable to develop an algorithmic approach with satisfactory success rate in recognition and tracking of objects on solar images.

The Wavetrack software is developed as part of the MOSAIICS project, VIHREN Program, funded by Bulgaria's National Science Fund. Find out more at https://mosaiics.astro.bas.bg/
Main developer of the code is Oleg Stepanyuk, with assistance from Kamen Kozarev and Mohamed Nedal.
(Stepanyuk. et al., J. Space Weather Space Clim. Vol. 12 (2022), https://doi.org/10.1051/swsc/2022020)

The issue with most algorithms currently used in solar feature detection and tracking is the complexity of their processing chains, which makes them difficult to implement; that they are usable for very specific tasks, and that often they are applicable only to data from a single instrument. 
What is more, these algorithms rely on pre-set intensity threshold parameters, and often do not include morphological information, such as shapes and multi-frequency appearance.
A number of algorithms for EUV wave detection of varying complexity exist (Podladchikova:2005, Verbeeck:2014, Long:2014, Ireland:2019) Of these perhaps the most advanced are the CorPITA (Long:2014) and AWARE (Ireland:2019). The CorPITA algorithm employs percentage base difference images to fit multi-Gaussian shapes to flare-source-centered, sector-averaged portions on the wave along the solar disk. The AWARE algorithm uses more advanced pre-processing in persistence running difference images, to characterize the wave shapes along similar flare-centered sectors, and a random sample consensus (RANSAC) algorithm to select the features.

Within Wavetrack processing routine, each time-step we start with base difference images which are constructed by subtracting base images from the current raw image of the sequence. This allows to enhance the change  in intensity caused by CBF, omit static details and reduce noise. In the next step, after choosing an appropriate intensity interval, the base difference or running difference image is decomposed with an A-Trous wavelet transform, where each wavelet coefficient is obtained by convolving the image array with a corresponding iteration of the wavelet kernel. The separate scales are obtained by subtracting wavelet coefficients, and each of them represents its own detailization level.
When the maximum value of the wavelet coefficients for a connected set of pixels satisfies certain conditions, this region is considered as a structure on the respective wavelet coefficient. Separate masks for each single object are obtained based on saddle points locations and gradient vectors. For that purpose we use Sobel-Feldman operator, a discrete differentiation operator, computing an approximation of the gradient of the image intensity function. The separate stand-alone object masks are obtained with a clustering algorithm, where the image fragments are iteratively split into quadrants and objects are renumbered according to the number of the quadrant they belong at each iteration.

To summarize, the processing sequence for a single timestep goes as follows (Using AIA data as an example): 

1) The Sunpy package is used to process the input data to AIA 1.5 level
2) Base Difference/Running Difference/Persistent Running Difference image is obtained
3) Certain part of the dynamic range is selected by cutting of the data above and below high and low threshold values from the input parameters
4) The image is decomposed with the a-trous wavelet transform
5) Relative thresholding (sigma units) is applied to each of the wavelet coefficients separately. The pixel intensities are assumed to follow a Gaussian distribution
6) Bit depth of the wavelet coefficients is reduced
7) Object masks are obtained via Pathway A or Pathway B 

( See algorithm block-scheme from Stepanyuk. et al (2022)) 
 
 7.1) In case of large-scale off-limb objects (coronal shock waves), a simplified processing scheme is usually enough. The object mask is obtained by direct reconstruction of the image from wavelet scales with certain weight coefficients, and one of the higher level scales is applied as a "reference mask".

 7.2) In more complex image processing cases, such as, for instance, an early-stage on-disk filament, complete utilization of multi-scale image decomposition approach would be necessary. Here, wavelet scale values serve as object criteria 

 For more details, please see Stepanyuk. et al., J. Space Weather Space Clim. Vol. 12 (2022)  https://doi.org/10.1051/swsc/2022020.

Typical input configuration for the Shock Wave recognition and tracking (see t_config.py)

class t_preproc_config(object): # preprocessing setup for Wavetrack ver. 0.40
    
    def __init__(self): 
       
       # Shows if base difference images to to be used.
       self.use_base_diff ='n' 
      
       # Differencing algorithm. Possible values are 'BASE_DIFF' 'RD', 'AWARE'
       
       # 1) 'BASE_DIFF' Base images are created by averaging series of images a few minutes prior
       # to the start of the eruption. Each time steps starts with base difference 
       # images which are constructed by subtracting base images from the current 
       # RAW image of the sequence.
       
       # 2) 'RD' Running difference or persistence running difference images (Ireland et al., 2019) 
       # are used when necessary to determine the moment when the eruption starts, or in case 
       # Wavetrack is used to study an active region or a filament.  Using running difference images 
       # introduces spurious features and is not recommended for discovering the true (projected) 
       # wave shape (Long et al., 2011)

       # 3) 'AWARE' The AWARE algorithm uses more advanced pre-processing  
       # persistence running  difference  images, to characterize the wave shapes  
       # along similar flare-centered sectors
       self.diff_algorithm = 'BASE_DIFF' 
       
       # Initial images are resized to x_size y_size specified below 
       self.x_size=512
       self.y_size=512
       
       # Solar disk parameters 
       # Coordinates and center of the solar disk is taken from disk_radius, disk_center_x, 
       # disk_center_y in case they are not red automatically from metadata. For such configuration
       # the parameter  use_fixed_sun_radius='n' should be set to 'n' (No)
       self.use_fixed_sun_radius='y' 
       self.disk_radius=45 
       self.disk_center_x=512
       self.disk_center_y=512
       
       # Use manual sun radius. To be red from metadata otherwise
       self.use_manual_rsun='y' 
       self.manual_rsun=45
  
       # Input data type. 'FITS' or 'raster'
       self.input_data_format='FITS' 
       # Input data level. Could be 'AIA_1.5', 'AIA_1', AIA_1_resized_log (experimental)
       self.data_lev='OTHER'
       
       # Load png images instead 
       self.use_preprocessed_png='y' # Testing Use at your own risk. 
       
       self.fits_extension = '.fts'

       # Threshold for absolute values while initial file read
       self.low_threshold=-10000
       self.high_threshold=40000
       # Threshold for base difference files
       self.low_threshold_bd= -10000
       self.high_threshold_bd=40000

       # Invert data during initial file read 
       self.data_invert_img_load='n'
       # Invert data after differencing 
       self.data_invert_pipeline='n'
      
       # Background image is processed in a parallel "thread"/object copy with separate pre- post- 
       # and decomposition parameters, and is used solely for better visual interpretation of the 
       # results, as a-trous wavelet method itself can work as an effective filtering technique 
       # See, the following parameters below:
       # self.bkg_resolution_bits,
       # self.bkg_thrhold_sigma,
       # self.bkg_grad_thrhold_sigma,
       # self.bkg_outside_thrhold_sigma,
       # self.bkg_scales_weights
       # 

       # Invert background images  
       self.data_invert_bkg='n'

       # 1) Use PIL library ("img") or numeric ("numeric") for image inversion for main image and 
       # second, background "thread" which is used for visual interpretation of the results 
       # 2) "numeric": numerical inversion of the array
       # Empirically, one way or another to be used depending on type of the input data
       self.data_invert_method='img' 
       self.data_invert_method_bkg='img'

       # Log initial image 
       self.adjust_dynrange='n'  
       self.dynrange_logbase=20

       # HERE ADD ON BACKGROUND IMAGES   

       # Load original (non differenced) data as background 
       self.bkg_load_original='y'
      
       # Image resize/rebin parameters
       self.resize='n'
       # Use manual rebin patch to adjust header metadata ? 
       self.manual_rebin_patch='n'
       # Use sunpy resample instead of manual fits/raster resize
       self.sunpy_resample='n'
       # Rebin factor. To be used together with rebin patch. 1024/4 = 256, for instance
       self.rebin_factor=4 
       
       # Input data with consequitive timesteps in FITS/raster format
       self.data_path='/data/'
       # FITS/raster data to create average base image. Usually 3-5 files 
       # a 2-5 minutes prior to the event 
       self.base_data_path='/base_data/'

       # Normalize data after Base/Running/Persistent(AWARE) differencing 
       self.normalize_init_data='y'
       # Circle mask to replace slar disk with zero pixels  
       self.apply_circle_mask='n'
       
       # Square mask to focus on a certain area of the image, ignoring the rest of the pixels
       # Useful when the region where the event takes place is known in advance
       self.x_1_sq = 100
       self.x_n_sq = 400
       self.y_1_sq = 0
       self.y_n_sq = 240

       # Artificially increase sun radius multiplying by rsun_coeff. Could be useful sometimes on 
       # a very bright images
       self.rsun_coeff = 1.00 # 1.04


class t_decompose_config(object): # decomposition setup

    
    def __init__(self):
        
        # An image bit depth determines the number of intensity levels to which an image could be 
        # theoretically resolved. Posterisation is the conversion of a continuous gradation of tone 
        # to several regions of fewer tones and happens naturally when image bit depth has been 
        # decreased.
       
        self.resolution_bits = 8

        # A-Trous Wavelet parameters. Amount of scales and weight coefficients to be used for image 
        # reconstruction 
        self.scales_n_max = 5
        self.scales_weights=[0,1,1,0,0,0,0,0,0]

   
        # Parameters for post-processing wavelet coefficients 
        
        # Additional PILLOW library parameters for PIL normalize and median filter 
        self.normalize='n'
        self.PIL_posterize='n' # Deprecated/not tested after v. 0.38. Use at your own risk.
        self.median_filt='n'   # Deprecated/not tested after v. 0.38. Use at your own risk   
        self.median_scale = 3  # Deprecated/not tested after v. 0.38. Use at your own risk
        self.PIL_posterize_ratio = 3 # Deprecated/not tested after ver 0.38. Use at your own risk
        
        # Work on scales, instead of wavelet coefficients
        self.use_scales_not_coeff='n' # Deprecated/not tested after ver 0.38. Use at your own risk

        # Parameters for feature recognition setup 
        # Gradient method 
        # The gradient field of the image is the result of applying the gradient operator to an image. 
        # In this case, the gradient field of the final object image (after the object mask is applied 
        # to the original image) is used for visual interpretation but can also serve as additional criteria 
        # for more robust object segmentation, as it highlights object contours, allows to determine saddle point
        # locations, and to calculate gradient vectors. Image gradients can also be used as part of training sets
        # for deep convolutional networks. By default, we use the Sobel-Feldman operator, a discrete differentiation 
        # operator, to compute an approximation of the gradient of the image intensity function.  At each pixel of 
        # the image, the result would be either the corresponding gradient vector, or the norm of this vector. 
        #  
        # kernel_sobel_gx= [[1,0,-1],
        #                  [2,0,-2],
        #                  [1,0,-1]]

        # kernel_sobel_gy= [[1,2,1],
        #                  [0,0,0],
        #                  [-1,-2,-1]]
   
        # Alternatively, a simple gradient could be applied (grad_method=Simple_grad ) 
        # self.kernel_grad = [[1,0,-1],
        #                    [0,0,0],
        #                    [-1,0,1]], 
        #
        # or curvature (grad_method=Hagenaer in each pixel point could be calculated according to 
        # Hagenaer et. al. 1999 Astr.J. 511:932-944,1999   
         
    
        self.grad_method='Sobel-Feldman'

        # Relative thresholding (statistics) 
        # Due to the significantly different statistical distributions
        # of pixel intensities, on-disk and off-limb parts of images are 
        # processed separately (apply_thrhold_circle_mask = 'y')
        # and the results are merged afterwards. 
        self.apply_thrhold_circle_mask = 'y'    

        # Threshold parameters for the limb (outside_thrhold_sigma), disk (thrhold_sigma) and 
        # gradient field (grad_thrhold_sigma). Sigma (standard deviation) units
        self.thrhold_sigma = 5.5
        self.grad_thrhold_sigma = 5.5
        self.outside_thrhold_sigma = 5.5 # 3.8
        # Minimum structure size in pixels. Smaller structures would be ommited 
        self.min_struct_size=400
        
        # Contour thickness
        self.cnt_thickness=16
 
        # Check which structures overlap within neighboring timesteps 
        self.search_overlap='y'
        
        # Split into intensity levels manually ( usually right setup of absolute thresholding )
        self.split_intensities = 'n'
        
        # Since each of the wavelet decomposition levels effectively represents its own 
        # image detail level separately, one of the higher decomposition levels (in practice fifth or sixth)
        # would reveal shapes of relatively large objects, almost completely omitting small scale
        # details and noise. After proper thresholding a binarised mask of a high decomposition scale is
        # applied to the joint structures mask as an additional filter. 
        # One of the higher levels of wavelet decomposition of the solar image is taken 
        # as a "reference mask", limiting the area where objects of interest are located 
        # by the shape of that mask.     
        self.reference_scale_n = 1  
        # Use coefficient (or scale otherwise)
        self.coeff_as_reference ='y'
        

        # Segmentation of the image is done by iteratively splitting it into quadrants while each 
        # object is renumbered in accordance with the quadrant number it belongs to at each iteration, 
        # provided the object does not cross the border of the quadrant. 
        # The value of the corresponding gradient field (and thus, a possible contour location) serves
        # as an additional criterion for object selection. Initial estimation of the objects count 
        # and quadrant locations is done via a Monte-Carlo algorithm. Alternatively, one-pass ABC mask 
        # could be used (experimental, not recommended) 
        self.use_abc_mask = 'n' 
        
        # Verbose output mode. Save all intermidiate steps, decompositions and structures output_log 
        # folder. Used mainly for testing purposes 
        self.verbose_output = 'y'
        self.save_scales = 'y'
        
        # The meaning of the following parameters are identical to main, non-background image
        # decomposition parameters 

        self.bkg_resolution_bits = 24
        self.bkg_thrhold_sigma=0.0
        self.bkg_grad_thrhold_sigma= 0.0
        self.bkg_outside_thrhold_sigma=0.0
        self.bkg_scales_weights=[0,1,1,1,1,1,1,1,0,0]
    
        
        # Output folders (within current folder)
        self.struct_save_path = '/output_structs/'
        self.objects_mla_save_path = '/output_objects_mla/'
        self.log_save_path = '/output_log/'
        self.grad_save_path = '/output_gradients/'
        self.results_coord_fname = '/output_structs/strucures_coord.txt'
        self.results_overlap_fname = '/output_structs/strucures_overlap.txt'
        self.trainset_save_path = '/trainset/'
        self.visualize_save_path = '/visualize/'

        
        # Window filter parameters. A final one-pass filter which eliminates small spurious 
        # details/noize
        self.window_filt_boxsize=10
        self.window_filt_increment=1
        self.window_filt_thrhold=0

  
        # Internal/debugging parameters. Not to be modified 
        self.override_w_bkg = 'n'
        self.override_bkg_w_orig = 'n'
        self.use_scales_not_coeff='n'
        self.optimal_w_coeff = 3
        self.objects_gradients = 'n'
        self.use_imsave='y'
        self.dist_per_pixel=1


preproc_xx.cfg, decompose_xx.cfg files can be used instead of modifying t_config.py files. 


Output: 


In it's verbose output mode the training set Wavetrack output for a single timestep looks as follows:
 
./trainset/
BASE_DIFF_data_t_029-0_2013-09-29T21:25:54.840.fits
BASE_DIFF_data_t_029-0_2013-09-29T21:25:54.840.png
masks_applied_BASE_DIFF_data_t_029-0_2013-09-29T21:25:54.840.fits
masks_applied_BASE_DIFF_data_t_029-0_2013-09-29T21:25:54.840.png
masks_applied_orig_data_t_029-0_2013-09-29T21:25:54.840.fits
masks_applied_orig_data_t_029-0_2013-09-29T21:25:54.840.png
masks_t_029-0_2013-09-29T21:25:54.840.fits
masks_t_029-0_2013-09-29T21:25:54.840.png
masks_cnt_t_029-0_2013-09-29T21:25:54.840.fits
masks_cnt_t_029-0_2013-09-29T21:25:54.840.png
orig_data_t_029-0_2013-09-29T21:25:54.840.fits
orig_data_t_029-0_2013-09-29T21:25:54.840.png
"

./vizualize/

bkg_bw_t_029-0_2013-09-29T21:25:54.840.png
bkg_t_029-0_2013-09-29T21:25:54.840.fits
masks_grads_projected_BASE_DIFF_data_bw_t_029-0_2013-09-29T21:25:54.840.png
masks_grads_projected_BASE_DIFF_data_t_029-0_2013-09-29T21:25:54.840.fits
masks_grads_projected_BASE_DIFF_data_t_029-0_2013-09-29T21:25:54.840.png
masks_grads_projected_bkg_bw_t_029-0_2013-09-29T21:25:54.840.png
masks_grads_t_029-0_2013-09-29T21:25:54.840.fits
masks_grads_t_029-0_2013-09-29T21:25:54.840.png
masks_projected_BASE_DIFF_data_bw_t_029-0_2013-09-29T21:25:54.840.png
masks_projected_BASE_DIFF_data_t_029-0_2013-09-29T21:25:54.840.fits
masks_projected_BASE_DIFF_data_t_029-0_2013-09-29T21:25:54.840.png
masks_projected_bkg_bw_t_029-0_2013-09-29T21:25:54.840.png
masks_raw_data_t_029-0_2013-09-29T21:25:54.840.fits
masks_raw_t_029-0_2013-09-29T21:25:54.840.png
masks_raw_w_grads_projected_bk
"

Each filename consists of
a) Type of the data it contains, i.e "masks"/"masks_raw_data"/"BASE_DIFF_DATA"/...
b) The wavetrack timestep — "029",
c) Object number within current timestep —  "0",
d) Date-time 2013-09-29T21:25:54.840 in standard form.

FITS files contain actual raw data, while png files are given for visual interpretation.


The examples below make use of the sample data, which can be downloaded directly from this link: https://mosaiics.astro.bas.bg/data/wavetrack_output_examples.tar.gz
