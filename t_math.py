import numpy as np
import numpy
from scipy import ndimage
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import numpy as np
import io
from PIL import Image, ImageOps
import sunpy.map
import cv2
from collections import deque
from scipy.signal import convolve2d
from skimage.transform import resize
from scipy.ndimage import gaussian_filter
from numpy.linalg import inv
from scipy.ndimage import convolve
import cv2
from scipy.signal import fftconvolve
from numpy.fft import fft2
from numpy import argmax
from scipy.stats import norm

class t_math(object):
#various substitute math and statistical routines   

    '''   
        1)     Lucas-Kanade (LK) Method:
        The Lucas-Kanade algorithm is a popular method for estimating optical flow. 
        It assumes that the pixel intensities in a local neighborhood remain constant between consecutive 
        frames. 

        The algorithm follows these steps:

        Feature Detection: Detect prominent features, such as corners or points of interest, in the first 
        frame using techniques like the Harris corner detector or Shi-Tomasi algorithm.

        Feature Tracking: For each feature detected in the first frame, the algorithm tries to track it 
        in the second frame by searching for the best match in a local neighborhood.

        Intensity Difference Calculation: Compute the intensity difference between the original feature 
        location and the corresponding location in the second frame.

        Optical Flow Estimation: Solve a set of linear equations using the least squares method to
        estimate the displacement (optical flow) that minimizes the intensity difference between the two frames.



        2) Horn-Schunck Method:
        
        The Horn-Schunck algorithm is an iterative method that estimates optical flow by assuming that 
        the motion field is smooth and the intensity changes are consistent across the entire image.
        The main steps involved in the Horn-Schunck method are as follows:
        Define the smoothness constraint: Assume that the flow field is smooth and can be represented by 
        a spatial gradient.

        Formulate an energy functional: Construct an energy function that incorporates both data term and 
        smoothness term. The data term measures the difference in intensities between the two frames, 
        while the smoothness term encourages smooth flow fields.

        Minimize the energy functional: Optimize the energy function by iteratively solving a set of
        partial differential equations using numerical techniques like Gauss-Seidel relaxation or 
        successive over-relaxation.

        Iterative Update: In each iteration, update the flow field by taking into account the neighboring 
        flow values and the local intensity gradients.



        The Horn-Schunck algorithm provides dense optical flow estimation, meaning it assigns a flow vector to each pixel. However, it assumes global smoothness and may produce inaccurate results in the presence of large displacements or occlusions.


    '''   

    def lucas_kanade_1d(self, prev_frame, curr_frame, window_size=15):
        
        # Define the gradient kernels
        sobel_x = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
        sobel_y = np.array([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])

        # Compute the image gradients
        prev_grad_x = np.convolve(prev_frame, sobel_x, mode='same')
        prev_grad_y = np.convolve(prev_frame, sobel_y, mode='same')

        # Initialize variables for optical flow, velocity field scalar, and velocity field vector
        optical_flow = np.zeros_like(prev_frame, dtype=np.float32)
        velocity_field_scalar = np.zeros_like(prev_frame, dtype=np.float32)
        velocity_field_vector = np.zeros((prev_frame.shape[0], prev_frame.shape[1], 2), dtype=np.float32)
        average_velocity = 0.0

        # Iterate over each pixel in the image
        for i in range(window_size, prev_frame.shape[0] - window_size):
            for j in range(window_size, prev_frame.shape[1] - window_size):
               # Extract the window from previous frame
               window_prev_x = prev_grad_x[i-window_size:i+window_size+1, j-window_size:j+window_size+1]
               window_prev_y = prev_grad_y[i-window_size:i+window_size+1, j-window_size:j+window_size+1]

               # Compute the elements of the Lucas-Kanade equation
               A = np.column_stack((window_prev_x.flatten(), window_prev_y.flatten()))
               b = -prev_frame[i-window_size:i+window_size+1, j-window_size:j+window_size+1].flatten()

               # Compute the least-squares solution
               velocity = np.linalg.lstsq(A, b, rcond=None)[0]

               # Update the optical flow, velocity field scalar, and velocity field vector
               optical_flow[i, j] = np.linalg.norm(velocity)
               velocity_field_scalar[i, j] = optical_flow[i, j]
               velocity_field_vector[i, j] = velocity

               # Accumulate the average velocity
               average_velocity += optical_flow[i, j]

        # Compute the average velocity
        average_velocity /= (prev_frame.shape[0] - 2 * window_size) * (prev_frame.shape[1] - 2 * window_size)


        return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity

    

    def horn_schunck_1d(self, prev_frame, curr_frame, alpha=1.0, num_iterations=100):

        # Convert the frames to grayscale
        prev_frame_gray = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)
        curr_frame_gray = cv2.cvtColor(curr_frame, cv2.COLOR_BGR2GRAY)
        
        # Initialize the optical flow, velocity scalar, and velocity vector fields
        optical_flow = np.zeros_like(prev_frame_gray, dtype=np.float32)
        velocity_field_scalar = np.zeros_like(prev_frame_gray, dtype=np.float32)
        velocity_field_vector = np.zeros_like(prev_frame_gray, dtype=np.float32)
        
        # Estimate the spatial gradients
        gradient_x = cv2.Sobel(prev_frame_gray, cv2.CV_32F, 1, 0, ksize=3)
        gradient_y = cv2.Sobel(prev_frame_gray, cv2.CV_32F, 0, 1, ksize=3)
        
        for _ in range(num_iterations):
            # Compute the temporal gradient between the frames
            temporal_gradient = curr_frame_gray - prev_frame_gray
            
            # Compute the weighted average of neighboring optical flow vectors
            avg_flow_x = cv2.filter2D(optical_flow, -1, np.array([[0, 0, 0], [0, 0.25, 0], [0, 0, 0]]))
            avg_flow_y = cv2.filter2D(optical_flow, -1, np.array([[0, 0, 0], [0, 0.25, 0], [0, 0, 0]]))
            
            # Compute the flow increment
            flow_increment = (gradient_x * avg_flow_x + gradient_y * avg_flow_y + temporal_gradient) / \
                             (alpha**2 + gradient_x**2 + gradient_y**2)
            
            # Update the optical flow
            optical_flow += flow_increment
        
        # Compute the magnitude of the optical flow as the velocity scalar
        velocity_field_scalar = np.linalg.norm(optical_flow, axis=2)
        
        # Compute the destination point using the optical flow as the velocity vector
        height, width = optical_flow.shape[:2]
        y_coords, x_coords = np.mgrid[0:height, 0:width]
        destination_points = np.stack((x_coords + optical_flow[:, :, 0], y_coords + optical_flow[:, :, 1]), axis=2)
        
        # Compute the average velocity scalar
        average_velocity = np.mean(velocity_field_scalar)
        
        return optical_flow, velocity_field_scalar, destination_points, average_velocity

    
    def fill_circle_with_zeros(self, arr, center, radius):
        """
        Fill a circle with zeros in a 2D numpy array.
    
        Parameters:
            arr (numpy.ndarray): 2D array to be modified.
            center (tuple): Center coordinates (row, column) of the circle.
            radius (int): Radius of the circle.
    
        Returns:
            numpy.ndarray: The modified array with the circle filled with zeros.
        """
        
        #x = input()
  
        rows, cols = arr.shape
        cx, cy = center
        arr_result = np.copy(arr)

        for y in range(max(0, cy - radius), min(rows, cy + radius + 1)):
            for x in range(max(0, cx - radius), min(cols, cx + radius + 1)):
                
                #dist=np.sqrt( (y-cy)*(col_i-col_cent) + (row_i-row_cent)*(row_i-row_cent) )     
                   
                #if dist>rad: 
                #    mask[col_i][row_i]=0


                if (x - cx) ** 2 + (y - cy) ** 2 <= radius ** 2:
                    arr_result[x,y] = 0

        return arr_result


    def resize_image(self, image, size_x, size_y):
        
        resized_image = resize(image, (size_x, size_y), anti_aliasing=True)
        return resized_image

    
    def data_norm(self, data):
        
        data_norm=(data - np.min(data)) / (np.max(data) - np.min(data)).astype(np.float32)
        return data_norm

    
    def calculate_sector_avg_velocity(self, optical_flow, dest_vec_x1, dest_vec_y1, dest_vec_x2, dest_vec_y2, incline_angle):
        # Calculate the reference vector
        dest_vector = np.array([dest_vec_x2 - dest_vec_x1, dest_vec_y2 - dest_vec_y1])
        
        # Normalize the reference vector
        dest_vector = dest_vector / (np.linalg.norm(dest_vector) + 1e-7)  # adding a small value to avoid division by zero
        
        # Calculate the magnitude and angle of the optical flow vectors
        magnitude, angle_of_flow = cv2.cartToPolar(optical_flow[..., 0], optical_flow[..., 1])
        
        # Calculate the angle of the destination vector
        angle_of_dest_vector = np.arctan2(dest_vector[1], dest_vector[0])
        
        # Calculate the angular difference between the flow vectors and the destination vector
        angular_difference = np.abs(angle_of_flow - angle_of_dest_vector)
        
        # Take into account that angles are periodic (0 and 2*pi are the same)
        angular_difference = np.minimum(angular_difference, 2 * np.pi - angular_difference)
        
        # Create a mask for flow vectors within the incline_angle from the destination vector
        motion_mask = angular_difference <= incline_angle
        
        # Calculate the average magnitude (velocity) for the vectors within the incline_angle
        average_velocity = np.mean(magnitude[motion_mask])
        
        # Extract the magnitudes of optical flow vectors that are within the sector
        sector_values = magnitude[motion_mask]
        
        return average_velocity, sector_values



    def compute_filtered_average(self, data, sigma_vlct):
        """
        Computes the average of the values in 'data' after removing values
        that are beyond 'sigma_vlct' standard deviations from the mean.
        
        Parameters:
            - data (numpy array): The input data
            - sigma_vlct (float): The number of standard deviations for the cut-off

        Returns:
            - average (float): The computed average after filtering
        """
     
        mean_val,std_val=norm.fit(data) 

        #mean_val = np.mean(data)
        #std_val = np.std(data)
        
        lower_bound = mean_val - sigma_vlct * std_val
        upper_bound = mean_val + sigma_vlct * std_val
        
        #lower_bound = sigma_vlct * std_val
        #upper_bound = sigma_vlct * std_val
        

        # Filtering values beyond the bounds
        filtered_data = np.zeros_like(data)


        #filtered_data_up = data[(data >= lower_bound)] 
        #filtered_data_low = data[(data <= upper_bound)]

        filtered_data_up = (data>lower_bound)*data  
        filtered_data_low = (data<upper_bound)*data


        filtered_data = np.add(filtered_data_up, filtered_data_low)
        #filtered_data = filtered_data_up
    
        # Computing the average of the remaining values
        #average = np.mean(filtered_data)
        average = np.mean(filtered_data[filtered_data.nonzero()])

        return filtered_data, average

    
    '''
    def lucas_kanade(self, img1_orig, img2_orig, feature_mask, window_size, size_x=None, size_y=None, sigma=1.0, sigma_2 = 16.0):

        img1_norm = self.data_norm(img1_orig)
        img2_norm = self.data_norm(img2_orig)

                
        # resize images if size parameters are given
        if size_x is not None and size_y is not None:
            img1_resize = self.resize_image(img1_norm, size_x, size_y)
            img2_resize = self.resize_image(img2_norm, size_x, size_y)
            feature_mask_resize = self.resize_image(feature_mask, size_x, size_y)
        else:
            img1_resize = img1_norm
            img2_resize = img2_norm
            feature_mask_resize = feature_mask
        
        #img1_resize *= feature_mask_resize
        #img2_resize *= feature_mask_resize    
    
        img1 = gaussian_filter(img1_resize, sigma=sigma)
        img2 = gaussian_filter(img2_resize, sigma=sigma)    

        kernel_x = np.array([[-1., 1.], [-1., 1.]]) * .25  # kernel for computing image derivatives 
        kernel_y = np.array([[-1., -1.], [1., 1.]]) * .25
        kernel_t = np.array([[1., 1.], [1., 1.]]) * .25
        w = window_size//2  # window_size is odd, all the pixels with offset in between [-w, w] are inside the window
        Ix = convolve(img1, kernel_x) + convolve(img2, kernel_x)
        Iy = convolve(img1, kernel_y) + convolve(img2, kernel_y)
        It = convolve(img1, kernel_t) + convolve(img2, -kernel_t)
        u = np.zeros_like(img1)
        v = np.zeros_like(img1)
       
        for y in range(w, Ix.shape[0]-w):
            for x in range(w, Ix.shape[1]-w):
                Ix_window = Ix[y-w:y+w+1, x-w:x+w+1].flatten()
                Iy_window = Iy[y-w:y+w+1, x-w:x+w+1].flatten()
                It_window = It[y-w:y+w+1, x-w:x+w+1].flatten()
                b = np.reshape(It_window, (It_window.shape[0], 1))  # get b here
                A = np.vstack((Ix_window, Iy_window)).T  # get A here
                if np.min(abs(np.linalg.eigvals(np.matmul(A.T, A)))) < 1e-3:  # if the smallest eigenvalue of A.T*A is small, then the solution does not exist, skip this window
                    continue
                nu = np.matmul(np.matmul(inv(np.matmul(A.T, A)), A.T), b)  # get velocity here
                u[y,x]=nu[0]
                v[y,x]=nu[1]

        optical_flow = (u, v)
        velocity_field_scalar = np.sqrt(u**2 + v**2) #* self.decompose_config.velocity_field_x
        velocity_field_vector = np.dstack((u, v))

        #average_velocity = np.mean(velocity_field_scalar)
        #average_velocity = np.mean(velocity_field_scalar[velocity_field_scalar.nonzero()])
        
        velocity_field_scalar_flt, average_velocity= self.compute_filtered_average(velocity_field_scalar, sigma_2)

        return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity
        #return optical_flow, velocity_field_scalar_flt, velocity_field_vector, average_velocity
    
    '''

    def lucas_kanade(self, img1_orig, img2_orig, feature_mask, window_size, size_x=None, size_y=None, sigma=1.0, sigma_2 = 16.0):
    #def lucas_kanade(self, img1_orig, img2_orig, window_size, size_x=None, size_y=None, sigma=1.0):

        img1_norm = self.data_norm(img1_orig)
        img2_norm = self.data_norm(img2_orig)
        
        # resize images if size parameters are given
        if size_x is not None and size_y is not None:
            img1_resize = self.resize_image(img1_norm, size_x, size_y)
            img2_resize = self.resize_image(img2_norm, size_x, size_y)
        else:
            img1_resize = img1_norm
            img2_resize = img2_norm

        img1 = gaussian_filter(img1_resize, sigma=sigma)
        img2 = gaussian_filter(img2_resize, sigma=sigma)    

        kernel_x = np.array([[-1., 1.], [-1., 1.]]) * .25  # kernel for computing image derivatives 
        kernel_y = np.array([[-1., -1.], [1., 1.]]) * .25
        kernel_t = np.array([[1., 1.], [1., 1.]]) * .25
        w = window_size//2  # window_size is odd, all the pixels with offset in between [-w, w] are inside the window
        Ix = convolve(img1, kernel_x) + convolve(img2, kernel_x)
        Iy = convolve(img1, kernel_y) + convolve(img2, kernel_y)
        It = convolve(img1, kernel_t) + convolve(img2, -kernel_t)
        u = np.zeros_like(img1)
        v = np.zeros_like(img1)
       
        for y in range(w, Ix.shape[0]-w):
            for x in range(w, Ix.shape[1]-w):
                Ix_window = Ix[y-w:y+w+1, x-w:x+w+1].flatten()
                Iy_window = Iy[y-w:y+w+1, x-w:x+w+1].flatten()
                It_window = It[y-w:y+w+1, x-w:x+w+1].flatten()
                b = np.reshape(It_window, (It_window.shape[0], 1))  # get b here
                A = np.vstack((Ix_window, Iy_window)).T  # get A here
                if np.min(abs(np.linalg.eigvals(np.matmul(A.T, A)))) < 1e-3:  # if the smallest eigenvalue of A.T*A is small, then the solution does not exist, skip this window
                    continue
                nu = np.matmul(np.matmul(inv(np.matmul(A.T, A)), A.T), b)  # get velocity here
                u[y,x]=nu[0]
                v[y,x]=nu[1]

        optical_flow = (u, v)
        velocity_field_scalar = np.sqrt(u**2 + v**2) #* self.decompose_config.velocity_field_x
        velocity_field_vector = np.dstack((u, v))

        #average_velocity = np.mean(velocity_field_scalar)
        average_velocity = np.mean(velocity_field_scalar[velocity_field_scalar.nonzero()])

        #velocity_field_scalar_flt, average_velocity= self.compute_filtered_average(velocity_field_scalar, sigma_2)

        return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity
    
    

    def horn_schunck(self, prev_frame_orig, curr_frame_orig, feature_mask, alpha, num_iterations, size_x, size_y, sigma, sigma_2 = 6):
        
        prev_frame_norm = self.data_norm(prev_frame_orig)
        curr_frame_norm = self.data_norm(curr_frame_orig)

        # Resize images if size parameters are given
        if size_x and size_y:
            prev_frame = self.resize_image(prev_frame_norm, size_x, size_y)
            curr_frame = self.resize_image(curr_frame_norm, size_x, size_y)
            feature_mask_resize = self.resize_image(feature_mask, size_x, size_y)
        else:
            prev_frame = prev_frame_norm
            curr_frame = curr_frame_norm
            feature_mask_resize = feature_mask

        # Apply Gaussian filter
        
        #prev_frame *= feature_mask_resize
        #curr_frame *= feature_mask_resize  

        prev_frame = gaussian_filter(prev_frame, sigma=sigma)
        curr_frame = gaussian_filter(curr_frame, sigma=sigma)

        # Sobel operators
        sobel_x = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]]) / 8
        sobel_y = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]]) / 8

        # Image gradients
        Ix = convolve(prev_frame, sobel_x)
        Iy = convolve(prev_frame, sobel_y)
        It = curr_frame - prev_frame

        # Initialize flow vectors
        u = np.zeros_like(prev_frame)
        v = np.zeros_like(prev_frame)

        # Iterative estimation
        for _ in range(num_iterations):
            u_avg = convolve(u, np.ones((3, 3)) / 9)
            v_avg = convolve(v, np.ones((3, 3)) / 9)

            u_denom = (Ix * u_avg + Iy * v_avg + It) / (alpha ** 2 + Ix ** 2 + Iy ** 2)
            v_denom = (Ix * u_avg + Iy * v_avg + It) / (alpha ** 2 + Ix ** 2 + Iy ** 2)

            u = u_avg - Ix * u_denom
            v = v_avg - Iy * v_denom

        optical_flow = np.sqrt(u**2 + v**2)
        velocity_field_scalar = optical_flow 
        velocity_field_vector = np.dstack((u, v))
        #average_velocity = np.mean(optical_flow)
        #average_velocity = np.mean(optical_flow[optical_flow.nonzero()])

        velocity_field_scalar_flt, average_velocity= self.compute_filtered_average(velocity_field_scalar, sigma_2)
        average_velocity = average_velocity * size_x 
        #return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity
        return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity

    



    def horn_schunck_old(self, prev_frame_orig, curr_frame_orig, alpha=1.0, num_iterations=100, size_x=None, size_y=None, sigma=1.0):
        
        if size_x and size_y:
            prev_frame = self.resize_image(prev_frame_orig, size_x, size_y)
            curr_frame = self.resize_image(curr_frame_orig, size_x, size_y)

        prev_frame = gaussian_filter(prev_frame, sigma=sigma)
        curr_frame = gaussian_filter(curr_frame, sigma=sigma)

        sobel_x = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
        sobel_y = np.array([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])

        prev_grad_x = convolve2d(prev_frame, sobel_x, mode='same')
        prev_grad_y = convolve2d(prev_frame, sobel_y, mode='same')

        curr_grad_x = convolve2d(curr_frame, sobel_x, mode='same')
        curr_grad_y = convolve2d(curr_frame, sobel_y, mode='same')

        velocity_field_x = np.zeros_like(prev_frame, dtype=np.float32)
        velocity_field_y = np.zeros_like(prev_frame, dtype=np.float32)

        for _ in range(num_iterations):
            vel_div = (velocity_field_x * curr_grad_x + velocity_field_y * curr_grad_y + prev_grad_x + prev_grad_y + curr_frame - prev_frame)
            vel_norm = alpha**2 + prev_grad_x**2 + prev_grad_y**2
            velocity_field_x -= vel_div * prev_grad_x / vel_norm
            velocity_field_y -= vel_div * prev_grad_y / vel_norm

        optical_flow = np.sqrt(velocity_field_x**2 + velocity_field_y**2)
        velocity_field_scalar = optical_flow
        velocity_field_vector = np.dstack((velocity_field_x, velocity_field_y))
        #average_velocity = np.mean(optical_flow)
        average_velocity = np.mean(optical_flow[optical_flow.nonzero()])
        return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity


    def calculate_average_velocity_given_angle(self, optical_flow, angle_value):
        
        # Get the magnitude and angle of the 2D vectors
        magnitude, angle = cv2.cartToPolar(optical_flow[..., 0], optical_flow[..., 1])
    
        # Create a unit vector in the direction of the given angle
        angle_vector = np.array([np.cos(angle_value), np.sin(angle_value)])

        # Calculate the dot product of the optical flow vectors and the angle vector
        dot_product = angle_vector[0] * optical_flow[..., 0] + angle_vector[1] * optical_flow[..., 1]
    
        # The dot product is positive for motion in the direction of the angle vector and negative in the opposite direction
        # Create a mask for pixels moving in the direction of the angle vector
        motion_mask = dot_product > 0

        # Calculate the average magnitude for these pixels
        average_velocity = np.mean(magnitude[motion_mask])
        # rework average_velocity = np.mean(optical_flow[optical_flow.nonzero()])

        return average_velocity


    
    def flct_full(self, prev_frame_orig, curr_frame_orig, window_size=15, size_x=None, size_y=None, sigma=1.0):    

        prev_frame_norm = self.data_norm(prev_frame_orig)
        curr_frame_norm = self.data_norm(curr_frame_orig)

        prev_frame = self.resize_image(prev_frame_norm, size_x, size_y) if size_x and size_y else prev_frame_orig
        curr_frame = self.resize_image(curr_frame_norm, size_x, size_y) if size_x and size_y else curr_frame_orig

        prev_frame = gaussian_filter(prev_frame, sigma=sigma)
        curr_frame = gaussian_filter(curr_frame, sigma=sigma)

        def local_correlation(template, image):
            correlation = fftconvolve(image, template[::-1, ::-1], mode='same')
            template_norm = np.sum(template**2)
            image_norm = fftconvolve(image**2, np.ones_like(template), mode='same')
            return correlation / (np.sqrt(template_norm * image_norm) + 1e-7)

        template = prev_frame[window_size:-window_size, window_size:-window_size]
        correlation_map = local_correlation(template, curr_frame)

        optical_flow = np.zeros_like(curr_frame, dtype=np.float32)
        velocity_field_scalar = np.zeros_like(curr_frame, dtype=np.float32)
        velocity_field_vector = np.zeros((curr_frame.shape[0], curr_frame.shape[1], 2), dtype=np.float32)
        average_velocity = 0.0

        for i in range(window_size, curr_frame.shape[0] - window_size):
            for j in range(window_size, curr_frame.shape[1] - window_size):
                window_corr = correlation_map[i-window_size:i+window_size+1, j-window_size:j+window_size+1]

                max_idx = np.unravel_index(np.argmax(window_corr), window_corr.shape)
                velocity = np.array(max_idx) - window_size

                optical_flow[i, j] = np.linalg.norm(velocity)
                velocity_field_scalar[i, j] = optical_flow[i, j]
                velocity_field_vector[i, j] = velocity
                average_velocity += optical_flow[i, j]

        average_velocity /= (curr_frame.shape[0] - 2 * window_size) * (curr_frame.shape[1] - 2 * window_size)

        return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity

    
    def flct_segment(self, prev_frame_orig, curr_frame_orig, window_size=15, size_x=None, size_y=None, sigma=1.0, x_1_sq=None, x_n_sq=None, y_1_sq=None, y_n_sq=None):

        prev_frame = self.resize_image(prev_frame_orig, size_x, size_y) if size_x and size_y else prev_frame_orig
        curr_frame = self.resize_image(curr_frame_orig, size_x, size_y) if size_x and size_y else curr_frame_orig

        prev_frame = gaussian_filter(prev_frame, sigma=sigma)
        curr_frame = gaussian_filter(curr_frame, sigma=sigma)

        def local_correlation(template, image):
            correlation = fftconvolve(image, template[::-1, ::-1], mode='same')
            template_norm = np.sum(template**2)
            image_norm = fftconvolve(image**2, np.ones_like(template), mode='same')
            return correlation / (np.sqrt(template_norm * image_norm) + 1e-7)

         # Process only the specified square window
        #prev_frame = prev_frame[y_1_sq:y_n_sq, x_1_sq:x_n_sq]
        #curr_frame = curr_frame[y_1_sq:y_n_sq, x_1_sq:x_n_sq]

        prev_frame = prev_frame[x_1_sq:x_n_sq, y_1_sq:y_n_sq]
        curr_frame = curr_frame[x_1_sq:x_n_sq, y_1_sq:y_n_sq]

        template = prev_frame[window_size:-window_size, window_size:-window_size]
        correlation_map = local_correlation(template, curr_frame)

        optical_flow = np.zeros_like(curr_frame, dtype=np.float32)
        velocity_field_scalar = np.zeros_like(curr_frame, dtype=np.float32)
        velocity_field_vector = np.zeros((curr_frame.shape[0], curr_frame.shape[1], 2), dtype=np.float32)
        average_velocity = 0.0

        for i in range(window_size, curr_frame.shape[0] - window_size):
            for j in range(window_size, curr_frame.shape[1] - window_size):
                window_corr = correlation_map[i-window_size:i+window_size+1, j-window_size:j+window_size+1]

                max_idx = np.unravel_index(np.argmax(window_corr), window_corr.shape)
                velocity = np.array(max_idx) - window_size

                optical_flow[i, j] = np.linalg.norm(velocity)
                velocity_field_scalar[i, j] = optical_flow[i, j]
                velocity_field_vector[i, j] = velocity
                average_velocity += optical_flow[i, j]

        average_velocity /= (curr_frame.shape[0] - 2 * window_size) * (curr_frame.shape[1] - 2 * window_size)

        return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity



    def flct(self, prev_frame_orig, curr_frame_orig, feature_mask_orig, window_size=15, size_x=None, size_y=None, sigma=1.0, sigma_2=6):
    #def flct(self, prev_frame_orig, curr_frame_orig, window_size=15, size_x=None, size_y=None, sigma=1.0):
        
        prev_frame_norm = self.data_norm(prev_frame_orig)
        curr_frame_norm = self.data_norm(curr_frame_orig)

        prev_frame = self.resize_image(prev_frame_norm, size_x, size_y) if size_x and size_y else prev_frame_orig
        curr_frame = self.resize_image(curr_frame_norm, size_x, size_y) if size_x and size_y else curr_frame_orig
        feature_mask = self.resize_image(feature_mask_orig, size_x, size_y) if size_x and size_y else feature_mask_orig
        

        prev_frame = gaussian_filter(prev_frame, sigma=sigma)
        curr_frame = gaussian_filter(curr_frame, sigma=sigma)

        
        #prev_frame = gaussian_filter(prev_frame, sigma=5)
        #curr_frame = gaussian_filter(curr_frame, sigma=5)


        def local_correlation(template, image):
            correlation = fftconvolve(image, template[::-1, ::-1], mode='same')
            template_norm = np.sum(template**2)
            image_norm = fftconvolve(image**2, np.ones_like(template), mode='same')
            norm = np.sqrt(template_norm * image_norm)
            return correlation / (norm + 1e-7) if np.any(norm) else correlation

        template = prev_frame[window_size:-window_size, window_size:-window_size]
        correlation_map = local_correlation(template, curr_frame)

        optical_flow = np.zeros_like(curr_frame, dtype=np.float32)
        velocity_field_scalar = np.zeros_like(curr_frame, dtype=np.float32)
        velocity_field_vector = np.zeros((curr_frame.shape[0], curr_frame.shape[1], 2), dtype=np.float32)

        for i in range(window_size, curr_frame.shape[0] - window_size):
            for j in range(window_size, curr_frame.shape[1] - window_size):
                window_corr = correlation_map[i-window_size:i+window_size+1, j-window_size:j+window_size+1]

                max_idx = np.unravel_index(np.argmax(window_corr), window_corr.shape)
                velocity = np.array(max_idx) - window_size

                optical_flow[i, j] = np.linalg.norm(velocity)
                velocity_field_scalar[i, j] = optical_flow[i, j]
                velocity_field_vector[i, j] = velocity

        # Calculate average_velocity considering only non-zero values
        #velocity_field_scalar_flt, average_velocity= self.compute_filtered_average(velocity_field_scalar, sigma_2)
         
        feature_mask_nonan = np.nan_to_num(feature_mask)
        velocity_field_scalar_flt = np.copy( np.multiply(velocity_field_scalar, feature_mask_nonan))
        velocity_field_scalar_flt = gaussian_filter(velocity_field_scalar_flt, sigma=sigma_2)
       
        
        average_velocity = np.mean(velocity_field_scalar_flt[velocity_field_scalar_flt.nonzero()])        
        #return optical_flow, velocity_field_scalar_flt, velocity_field_vector, average_velocity
        return optical_flow, feature_mask_nonan, velocity_field_vector, average_velocity
         

    def farneback(self, prev_frame_orig, curr_frame_orig, feature_mask, size_x , size_y , sigma=1.0, sigma_2 = 6):
        
        prev_frame_norm = self.data_norm(prev_frame_orig)
        curr_frame_norm = self.data_norm(curr_frame_orig)

        prev_frame_resize = self.resize_image(prev_frame_norm, size_x, size_y) if size_x and size_y else prev_frame_orig
        curr_frame_resize = self.resize_image(curr_frame_norm, size_x, size_y) if size_x and size_y else curr_frame_orig
        feature_mask_resize = self.resize_image(feature_mask, size_x, size_y) if size_x and size_y else feature_mask
     

        prev_frame_resize *= feature_mask_resize
        curr_frame_resize *= feature_mask_resize  

        prev_frame = gaussian_filter(prev_frame_resize, sigma=sigma)
        curr_frame = gaussian_filter(curr_frame_resize, sigma=sigma)


        # Ensure the frames are in float32 format
        #prev_frame = prev_frame.astype(np.float32)
        #next_frame = next_frame.astype(np.float32)

        # Calculate optical flow
        flow = cv2.calcOpticalFlowFarneback(prev_frame, curr_frame, None, 0.5, 3, 15, 3, 5, 1.2, 0)

        # Get the magnitude and angle of the 2D vectors
        magnitude, angle = cv2.cartToPolar(flow[..., 0], flow[..., 1])

        # Create the vector destination points (endpoints of the vectors)
        h, w = flow.shape[:2]
        y, x = np.mgrid[0:h, 0:w]
        vector_destinations = np.dstack((x + flow[..., 0], y + flow[..., 1])).astype(np.float32)

        # Calculate the average velocity
        #average_velocity = np.mean(magnitude)
        #average_velocity = np.mean(magnitude[magnitude.nonzero()])
        
        velocity_field_scalar_flt, average_velocity= self.compute_filtered_average(magnitude, sigma_2)
        average_velocity = average_velocity * size_x * size_y 
        #return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity
        return flow, velocity_field_scalar_flt, vector_destinations, average_velocity

        #return flow, magnitude, vector_destinations, average_velocity

               
    def horn_schunck_old(self, prev_frame_orig, curr_frame_orig, alpha=1.0, num_iterations=100, size_x=None, size_y=None, sigma=1.0):
        
        if size_x and size_y:
            prev_frame = self.resize_image(prev_frame_orig, size_x, size_y)
            curr_frame = self.resize_image(curr_frame_orig, size_x, size_y)

        prev_frame = gaussian_filter(prev_frame, sigma=sigma)
        curr_frame = gaussian_filter(curr_frame, sigma=sigma)

        sobel_x = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
        sobel_y = np.array([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])

        prev_grad_x = convolve2d(prev_frame, sobel_x, mode='same')
        prev_grad_y = convolve2d(prev_frame, sobel_y, mode='same')

        curr_grad_x = convolve2d(curr_frame, sobel_x, mode='same')
        curr_grad_y = convolve2d(curr_frame, sobel_y, mode='same')

        velocity_field_x = np.zeros_like(prev_frame, dtype=np.float32)
        velocity_field_y = np.zeros_like(prev_frame, dtype=np.float32)

        for _ in range(num_iterations):
            vel_div = (velocity_field_x * curr_grad_x + velocity_field_y * curr_grad_y + prev_grad_x + prev_grad_y + curr_frame - prev_frame)
            vel_norm = alpha**2 + prev_grad_x**2 + prev_grad_y**2
            velocity_field_x -= vel_div * prev_grad_x / vel_norm
            velocity_field_y -= vel_div * prev_grad_y / vel_norm

        optical_flow = np.sqrt(velocity_field_x**2 + velocity_field_y**2)
        velocity_field_scalar = optical_flow
        velocity_field_vector = np.dstack((velocity_field_x, velocity_field_y))
        #average_velocity = np.mean(optical_flow)
        average_velocity = np.mean(optical_flow[optical_flow.nonzero()])
        
        return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity


    def flct_2(self, prev_frame, next_frame):
        """
        Function to implement Fourier Local Correlation Tracking.
        Returns:
        – velocity field scalar values
        – velocity field vector destinations
        – average velocity
        """
        # Calculate magnitude of 2D fourier transform
        fourier_magnitude = abs(fft2(prev_frame))

        # Find peaks in magnitude image using Gaussian filter
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
        filtered_magnitude = cv2.filter2D(fourier_magnitude, -1, kernel)
        #peaks = argmax(filtered_magnitude)
        peaks = np.array(np.unravel_index(np.argmax(filtered_magnitude), fourier_magnitude.shape)).T


        velocity_field_vector = []
        velocity_field_scalar = []

        for i in range(len(peaks)):

            # Find peak coordinates in next frame
            peaks_next = argmax(abs(fft2(next_frame)))[peaks[i]]

            # If peak is present in next frame, append its coordinates
            velocity = np.array(peaks_next) - np.array(peaks[i])
            velocity_field_vector.append(velocity)
            velocity_field_scalar.append(np.linalg.norm(velocity))

        # Average velocity over the frame
        #average_velocity = np.mean(velocity_field_scalar)
        average_velocity = np.mean(velocity_field_scalar[velocity_field_scalar.nonzero()])

        return velocity_field_scalar, velocity_field_vector, average_velocity


    def gauss(self,x, *p):
    # Gaussian 
        A, mu, sigma = p
        return A*numpy.exp(-(x-mu)**2/(2.*sigma**2))
        # Define some test data which is close to Gaussian
    

    def gauss_fit(self,data):
    # Gaussian fit
  
        hist, bin_edges = numpy.histogram(data, density=True)
        bin_centres = (bin_edges[:-1] + bin_edges[1:])/2

        # p0 is the initial guess for the fitting coefficients (A, mu and sigma above)
        p0 = [1., 0., 1.]

        coeff, var_matrix = curve_fit(self.gauss, bin_centres, hist, p0=p0)
        # Get the fitted curve
        hist_fit = self.gauss(bin_centres, *coeff)

        fitted_mean,fitted_std=norm.fit(data) #to compare 
        return hist,hist_fit,fitted_mean,fitted_std

    
    def convolve_list_arr(self,source,kernel):           
    # convolve lists function, as np supports only arrays convolution         
        source_arr=np.array(source)
        kernel_arr=np.array(kernel)
        result_arr=ndimage.convolve(np.array(source),np.array(kernel), mode='reflect')
        return result_arr


    def convolve_list(self,source,kernel):           
        return list(self.convolve_list_arr(source,kernel))

   
    def rgb_2_gray(self,rgb):
        r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
        gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
        return gray


    def circle_mask(self,col_n,row_n,col_cent,row_cent, rad): # Deprecated, slow version. Rewrite        
        mask = [[1 for col_i in range(col_n)] for row_i in range(row_n)]

        for col_i in range(0,col_n):  
            for row_i  in range(0,row_n):
                   
                dist=np.sqrt( (col_i-col_cent)*(col_i-col_cent) + (row_i-row_cent)*(row_i-row_cent) )     
                   
                if dist>rad: 
                    mask[col_i][row_i]=0

        return mask


    def square_mask(self,col_n,col_m,row_n,row_m,col_max,row_max ):

        mask = [[0 for col_i in range(col_max)] for row_i in range(row_max)]

        for col_i in range(col_n,col_m):  
            for row_i  in range(row_n,row_m):
                    mask[col_i][row_i]=1

        return mask

    
    def export_resize_fig_matplotlib(self, plot, f_name, size_x,size_y):
       
        #fig = plt.figure(frameon=False)
        fig=plot.figure
        f_dpi=fig.dpi

        fig.set_size_inches(size_x/f_dpi, size_y/f_dpi)

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)
        fig.savefig(f_name, dpi=f_dpi )
 
        io_buf = io.BytesIO()
        fig.savefig(io_buf, format='raw', dpi=f_dpi)
        io_buf.seek(0)                
        io_buff_arr=np.frombuffer(io_buf.getvalue())

        return io_buff_arr
    

    def merge_png_layers(self, fname_bkg, fname_fg, fname_result): 

        from PIL import Image

        background = Image.open(fname_bkg)
        foreground = Image.open(fname_fg)
        
        foreground.convert('RGBA')
        background.convert('RGBA')
        
        final_2 = Image.new("RGBA", background.size)
        final_2.paste(background, (0,0), background)
        final_2.paste(foreground, (0,0), foreground)
        final_2.save(fname_result)


    def merge_png_layers_transp_r(self, fname_bkg, fname_fg, fname_result): 
        # Merge two png's with transparancy
        from PIL import Image
        bkg_src = Image.open(fname_bkg)
        fg_src = Image.open(fname_fg)
         
        bkg = bkg_src.convert('RGB')
        fg = fg_src.convert('RGB') 
        #r_bkg, g_bkg, b_bkg = np.divide(bkg.split(),256)
        #r_fg, g_fg, b_fg = np.divide(fg.split(),256)
        size_x = np.size(bkg,0)
        size_y = np.size(bkg,1)

        r_bkg_img, g_bkg_img, b_bkg_img = bkg.split()
        r_fg_img, g_fg_img, b_fg_img = fg.split()

              
        r_bkg = np.divide(np.array(r_bkg_img),256)  
        g_bkg = np.divide(np.array(g_bkg_img),256)
        b_bkg = np.divide(np.array(b_bkg_img),256)
        r_fg = np.divide(np.array(r_fg_img),256)
        g_fg = np.divide(np.array(g_fg_img),256) 
        b_fg = np.divide(np.array(b_fg_img),256) 


        r_fg_coeff=np.multiply(r_fg,0.299)
        g_fg_coeff=np.multiply(g_fg,0.587)
        b_fg_coeff=np.multiply(b_fg,0.114)      

        gs_fg=np.array(r_fg_coeff)
        gs_fg=np.add(gs_fg,g_fg_coeff)
        gs_fg=np.add(gs_fg,b_fg_coeff)
          
        r_fin_float=np.multiply(r_bkg,gs_fg)
        r_fin=np.uint8(np.multiply(r_fin_float,255.9999))
        g_fin=np.uint8(np.multiply(g_bkg,255.9999))
        b_fin=np.uint8(np.multiply(b_bkg,255.9999))
        
        rgb_fin = np.zeros((size_x,size_y,3), 'uint8')
        rgb_fin[..., 0] = r_fin
        rgb_fin[..., 1] = g_fin
        rgb_fin[..., 2] = b_fin
        img_fin = Image.fromarray(rgb_fin)

        img_fin.save(fname_result, 'PNG')
        
        
    def merge_two_layers_transp_r_save_png(self, bkg_arr, fg_arr, fname_result): 

        from PIL import Image
        #bkg_arr_max=np.amax(bkg_arr)
        
        size_x = np.size(bkg_arr,0)
        size_y = np.size(bkg_arr,1)

        bkg_arr_max=np.amax(bkg_arr)
        fg_arr_max = np.amax(fg_arr)
        
        bkg_arr_norm=np.divide(bkg_arr,bkg_arr_max) #np.subtract(1,np.divide(bkg_arr,bkg_arr_max)) 

        fg_arr_norm=np.divide(fg_arr,fg_arr_max) 


        fg_arr_r = np.array(fg_arr_norm)
        fg_arr_g = np.subtract(1,fg_arr_norm)
        fg_arr_b = np.subtract(1,fg_arr_norm) 
        
        fin_r_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_r),2)
        fin_g_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_g),2)
        fin_b_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_b),2)

        fin_r = np.array(bkg_arr_norm)
        fin_g = np.array(bkg_arr_norm)
        fin_b = np.array(bkg_arr_norm)
        
        fin_r_mask = (fg_arr_norm>0)*fin_r_all_px
        fin_g_mask = (fg_arr_norm>0)*fin_g_all_px
        fin_b_mask = (fg_arr_norm>0)*fin_b_all_px 
       
        fin_r_bkg = (fg_arr_norm<=0)*bkg_arr_norm
        fin_g_bkg = (fg_arr_norm<=0)*bkg_arr_norm
        fin_b_bkg = (fg_arr_norm<=0)*bkg_arr_norm
        
        fin_r=np.add(fin_r_mask,fin_r_bkg)
        fin_g=np.add(fin_g_mask,fin_g_bkg)
        fin_b=np.add(fin_b_mask,fin_b_bkg)


        fin_r_256=np.uint8(np.multiply(fin_r,255.9999))
        fin_g_256=np.uint8(np.multiply(fin_g,255.9999))
        fin_b_256=np.uint8(np.multiply(fin_b,255.9999))
        
        fin_rgb = np.zeros((size_x,size_y,3), 'uint8')
        
        fin_rgb[..., 0] = fin_r_256
        fin_rgb[..., 1] = fin_g_256
        fin_rgb[..., 2] = fin_b_256

        img_fin = Image.fromarray(fin_rgb)
        img_fin_flip=ImageOps.flip(img_fin)
        img_fin_flip.save(fname_result)



    def merge_three_layers_transp_r_save_png(self, bkg_arr, fg_arr_1, fg_arr_2, fname_result): 
       
        size_x = np.size(bkg_arr,0)
        size_y = np.size(bkg_arr,1)

        bkg_arr_max=np.amax(bkg_arr)
        fg_arr_1_max = np.amax(fg_arr_1)
        fg_arr_2_max = np.amax(fg_arr_2)
        
        bkg_arr_norm=np.divide(bkg_arr,bkg_arr_max) #np.subtract(1,np.divide(bkg_arr,bkg_arr_max)) 

        fg_arr_1_norm=np.divide(fg_arr_1,fg_arr_1_max) 
        fg_arr_2_norm=np.divide(fg_arr_2,fg_arr_2_max)

        fg_arr_1_r = np.array(fg_arr_1_norm)
        fg_arr_1_g = np.subtract(1,fg_arr_1_norm)
        fg_arr_1_b = np.subtract(1,fg_arr_1_norm) 

        fg_arr_2_r = np.array(fg_arr_2_norm)
        fg_arr_2_g = np.subtract(1,fg_arr_2_norm)
        fg_arr_2_b = np.subtract(1,fg_arr_2_norm)
                
        fg_arr_r = np.divide(np.add(fg_arr_2_r,fg_arr_1_r),2)
        fg_arr_g = np.divide(np.add(fg_arr_2_g,fg_arr_1_g),2)
        fg_arr_b = np.divide(np.add(fg_arr_2_b,fg_arr_1_b),2)
            
        fin_r_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_r),2)
        fin_g_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_g),2)
        fin_b_all_px = np.divide(np.add(bkg_arr_norm,fg_arr_b),2)

        fin_r = np.array(bkg_arr_norm)
        fin_g = np.array(bkg_arr_norm)
        fin_b = np.array(bkg_arr_norm)

        fin_r_mask = (fg_arr_1_norm>0)*fin_r_all_px
        fin_g_mask = (fg_arr_1_norm>0)*fin_g_all_px
        fin_b_mask = (fg_arr_1_norm>0)*fin_b_all_px 
       
        fin_r_bkg = (fg_arr_1_norm<=0)*bkg_arr_norm
        fin_g_bkg = (fg_arr_1_norm<=0)*bkg_arr_norm
        fin_b_bkg = (fg_arr_1_norm<=0)*bkg_arr_norm
        
        fin_r=np.add(fin_r_mask, fin_r_bkg)
        fin_g=np.add(fin_g_mask ,fin_g_bkg)
        fin_b=np.add(fin_b_mask ,fin_b_bkg)


        fin_r_256=np.uint8(np.multiply(fin_r,255.9999))
        fin_g_256=np.uint8(np.multiply(fin_g,255.9999))
        fin_b_256=np.uint8(np.multiply(fin_b,255.9999))
        
        fin_rgb = np.zeros((size_x,size_y,3), 'uint8')
        
        fin_rgb[..., 0] = fin_r_256
        fin_rgb[..., 1] = fin_g_256
        fin_rgb[..., 2] = fin_b_256

        img_fin = Image.fromarray(fin_rgb)
        img_fin_flip=ImageOps.flip(img_fin)
        img_fin_flip.save(fname_result)


    def save_data_pic(self, data, f_name,size_x, size_y):
    
        fig=plt.figure()
        fig.max_open_warning=40
        fig = plt.figure(figsize=(10,10), dpi=400)
        plt.axis('off')
        plot=plt.imshow(data, cmap='gray')
        fig=plot.figure
        f_dpi=fig.dpi

        fig.set_size_inches(size_x/f_dpi, size_y/f_dpi)
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)

        fig.savefig(f_name, dpi=f_dpi )
        fig.clear()
 
        plt.close()
      


    def merge_two_fits(self,fname_1, fname_2, fname_result):

        #LOAD APPLIED MASK FILE (ALREADY RESAMPLED)
        bkg_map=sunpy.map.Map(fname_1)
        fg_map=sunpy.map.Map(fname_2)

        #PLOT THE ORIGINAL DATA INVERTED AND THE APPLIED MASK ON TOP WITH A DIFFERENT COLOR MAP
        fig = plt.figure(figsize=(8,8))
        bkg_map.plot(cmap='Greys')
        #plt.imshow(bkg_map.data, cmap='gray')
        fg_map.data[fg_map.data == 0.] = np.nan
        fg_map.plot(cmap='rainbow')
        fig.savefig(fname_result)
        plt.close()

        

    def adjust_dynrange_data(self, data_arr, base):

        data_max=np.amax(data_arr) 
        c = data_max/(np.log(1.1+data_max))   
        data_arr_plus = np.add(data_arr,1.1)
        data_arr_log = np.log(data_arr_plus)
        data_arr_log_base = np.divide(data_arr_log, np.log(base))      
        data_arr_log_norm = np.multiply(data_arr_log, c)

        #logmax=np.log(data_arr)/np.log(base)
        #data_arr_max=np.amax(data_arr)
        #data_arr_log=np.divide(np.log(data_arr),np.log(base))
        #data_arr_log_norm=np.multiply(data_arr_log,(data_arr_max/logmax))
        #data_arr_log_norm=np.multiply((data_arr),(data_arr))
        #data_arr_log = c* np.log(1+data_arr)
        #log  c* np.log(1+data_arr)
        #c = 255 / (log (1 + max_input_pixel_value))
        return data_arr_log_norm
        
        
 
    def invert_data_numeric(self, data_arr): 
        
        data_arr_max=np.amax(data_arr)
        data_img_inverted_c=np.subtract(data_arr_max,data_arr)
        return np.asarray(data_img_inverted_c)
        #hint: np.subtract(1,np.divide(bkg_arr,bkg_arr_max))


    def invert_data_img(self, data_arr): 
        
        #data_image=Image.fromarray(data_arr,'L') # try L option         
        data_image=Image.fromarray(data_arr) 
        data_image_RGB=data_image.convert('RGB')
        data_img_inverted = ImageOps.invert(data_image_RGB)
        #data_img_inverted_c=data_img_inverted.convert('L')
        #data_img_inverted_b=ImageOps.invert(data_img_inverted)
        data_img_inverted_c=data_img_inverted.convert('L')
        data_img_inverted_c.save('invert_log.png')
        return np.asarray(data_img_inverted_c)



    def bring_to_int32(self,data_arr):
        
        int32ii=np.iinfo(np.int32)
        data_arr_max = np.amax(data_arr) 
        data_arr_adj_f = np.round(np.multiply(data_arr,(int32ii.max/4)/data_arr_max))
        #/4 to avoid clapping after data is added/multiplied
        return data_arr_adj_f.astype(np.int32)



    def bring_to_one_float64(self,data_arr):    
    
        data_arr_max = np.amax(data_arr) 
        data_arr_adj_f = np.multiply(data_arr,1/data_arr_max)
        return data_arr_adj_f.astype(np.float64)

    
    def normalize_posterize(self,data, bits):
         print('Normalizing data, bringing to '+str(bits)+' bits resolution...')
         int32ii=np.iinfo(np.int32)
         data_min=np.amin(data) 
         data_plus= np.subtract(data,data_min)
         ratio= np.amax(data_plus)/pow(2,bits) 
         data_div_f = np.divide(data_plus,ratio)
         data_div_f_r = np.round(data_div_f)
         print('Normalizing/bringing the data to [0...'+ str( (int32ii.max/4)  )+'] scale...'  )
         data_norm_f = np.round(np.multiply(data_div_f_r.astype(np.int32), (int32ii.max/4)/pow(2,bits)))
         data_norm = data_norm_f.astype(np.int32)
         return data_norm
    
    
    def set_obj_prop_config_file(self, object,fname): 
           
        file_id = open(fname, 'r')
        lines = file_id.readlines()
      
        for line in lines:
            
            term_1, term_2 = line.split(',')
            
            try: 
               value = float(term_2)
               if (value % 1 == 0):
                   value = int(term_2)

            except ValueError:
                   value = str(term_2)


            if hasattr(object,term_1):
               
                object.__dict__[term_1] = value
            
            else: 

                print("Warning, unable to process config line: " + str(line)) 

        file_id.close()

    
    ''' 
    ver 41, far countour routines to pick up the forefront

    This implementation defines the following functions:

    depth_first_search: Performs DFS on the input image array to find connected regions of 1s.
    get_regions: Calls depth_first_search to find all connected regions in the input image array.
    make_contour_fat: Makes the contour fatter by adding a specified thickness around each point in the contour.
    draw_fat_contour: Draws the fat contour on a new image array.
    outer_contour_only: Finds the largest connected region and returns a new image array with only the outer conto

    '''
    '''
    def depth_first_search(self,x, y, visited, img_array):
        if x < 0 or y < 0 or x >= img_array.shape[1] or y >= img_array.shape[0] or visited[y, x] or img_array[y, x] == 0:
            return []
    
        visited[y, x] = True
        region = [(x, y)]
        
        for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            region += self.depth_first_search(x + dx, y + dy, visited, img_array)
        
        return region
    '''
    '''
    def depth_first_search(self, x, y, visited, img_array):
        
        stack = [(x, y)]
        region = []
    
        while stack:
            x, y = stack.pop()
        
            if (0 <= x < img_array.shape[1] and 0 <= y < img_array.shape[0] and
                not visited[y, x] and img_array[y, x] == 1):
                visited[y, x] = True
                region.append((x, y))
                for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                    stack.append((x + dx, y + dy))
        return region


    def get_regions(self,img_array):
     
        visited = np.zeros_like(img_array, dtype=bool)
        regions = []
        for y in range(img_array.shape[0]):
            for x in range(img_array.shape[1]):
                if not visited[y, x] and img_array[y, x] == 1:
                    regions.append(self.depth_first_search(x, y, visited, img_array))
       
        return regions


    def make_contour_fat(self,contour, thickness):
    
        fat_contour = []
        for point in contour:
            x, y = point
            for i in range(-thickness // 2, thickness // 2 + 1):
                for j in range(-thickness // 2, thickness // 2 + 1):
                    fat_contour.append((x + i, y + j))
        
        return fat_contour



    def draw_fat_contour(self,img_array, contour, thickness, color):
       
        for point in contour:
            x, y = point
            if 0 <= x < img_array.shape[1] and 0 <= y < img_array.shape[0]:
                img_array[y, x] = color


    def outer_contour_only(self,img_array, thickness=5):
        
        regions = self.get_regions(img_array)
        largest_region = max(regions, key=len)
        fat_contour = self.make_contour_fat(largest_region, thickness)
        new_img_array = np.zeros_like(img_array)
        self.draw_fat_contour(new_img_array, fat_contour, thickness, 1)
        return new_img_array

    '''

    

    '''
    def get_outer_contours(self, img):
        
        contours, _ = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        return contours


    def is_outer_pixel(self, point, center, img_array):
        
        x, y = point
        cx, cy = center
        h, w = img_array.shape

        dx = x - cx
        dy = y - cy

        while 0 <= x < w and 0 <= y < h:
            x += dx
            y += dy
            if 0 <= x < w and 0 <= y < h and img_array[y, x] == 1:
                return False
        return True


    def make_contour_fat(self, contour, thickness, center, img_array):
        
        fat_contour = []
        for point in contour:
            x, y = point[0]
            if self.is_outer_pixel((x, y), center, img_array):
                for i in range(-thickness // 2, thickness // 2 + 1):
                    for j in range(-thickness // 2, thickness // 2 + 1):
                        fat_contour.append([x + i, y + j])
        return np.array(fat_contour)

    def draw_fat_contour(self, img, contour, thickness, color):
        
        for point in contour:
            x, y = point
            img[y, x] = color

    def get_center(self, img):
        
        h, w = img.shape[:2]
        return w // 2, h // 2


    def outer_contour_only(self, img_array, thickness=5):
        binarized_array = (img_array * 255).astype(np.uint8)
        outer_contours = self.get_outer_contours(binarized_array)
        new_array = np.zeros_like(binarized_array)
        center = self.get_center(binarized_array)
    
        for contour in outer_contours:
            fat_outer_contour = self.make_contour_fat(contour, thickness, center, binarized_array)
            for point in fat_outer_contour:
                x, y = point
                if 0 <= x < new_array.shape[1] and 0 <= y < new_array.shape[0]:
                    new_array[y, x] = 1
        
        return new_array
    '''
 
    '''
    def has_non_zero_pixels_away_from_center(self, point, center, img_array):
       
        x, y = point
        cx, cy = center
        h, w = img_array.shape

        dx = cx - x
        dy = cy - y

        if dx != 0:
            x_step = 1 if dx > 0 else -1
        else:
            x_step = 0

        if dy != 0:
            y_step = 1 if dy > 0 else -1
        else:
            y_step = 0

        x += x_step
        y += y_step

        while 0 <= x < w and 0 <= y < h:
            if img_array[y, x] != 0:
                return True
            x += x_step
            y += y_step

        return False
    '''
    
    def has_non_zero_pixels_away_from_center(self, point, center, img_array):
    
        x, y = point
        cx, cy = center
        h, w = img_array.shape

        dx = x - cx
        dy = y - cy

        if dx != 0:
            x_step = 1 if dx > 0 else -1
        else:
            x_step = 0

        if dy != 0:
            y_step = 1 if dy > 0 else -1
        else:
            y_step = 0

        x += x_step
        y += y_step

        while 0 <= x < w and 0 <= y < h:
            if img_array[y, x] != 0:
                return True
            x += x_step
            y += y_step

        return False


    def is_outer_pixel(self, point, center, img_array):
        return not self.has_non_zero_pixels_away_from_center(point, center, img_array)


    def get_outer_contours(self, img):
        
        contours, _ = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        return contours


    '''
    def make_contour_fat(self, contour, thickness, center, img_array):
        
        fat_contour = []
        for point in contour:
            x, y = point[0]
            if self.is_outer_pixel((x, y), center, img_array):
                for i in range(-thickness // 2, thickness // 2 + 1):
                    for j in range(-thickness // 2, thickness // 2 + 1):
                        if 0 <= x + i < img_array.shape[1] and 0 <= y + j < img_array.shape[0]:
                            fat_contour.append([x + i, y + j])
        return np.array(fat_contour)
    '''


    def draw_fat_contour(self, img, contour, thickness, color):
    
        for point in contour:
            x, y = point
            if 0 <= x < img.shape[1] and 0 <= y < img.shape[0]:
                img[y, x] = color

    
    def make_contour_fat(self, contour, thickness, center, img_array):
        
        fat_contour = []
        for point in contour:
            x, y = point[0]
            if self.is_outer_pixel((x, y), center, img_array):
                for i in range(-thickness // 2, thickness // 2 + 1):
                    for j in range(-thickness // 2, thickness // 2 + 1):
                        if 0 <= x + i < img_array.shape[1] and 0 <= y + j < img_array.shape[0]:
                            if img_array[y + j, x + i] > 0 :  # check if pixel is inside the object
                                fat_contour.append([x + i, y + j])
        return np.array(fat_contour)


    def get_center(self, img):
        
        h, w = img.shape[:2]
        return w // 2, h // 2

    
    def outer_contour_only(self, img_array, thickness=5):
    
        binarized_array = (img_array * 255).astype(np.uint8)
        outer_contours = self.get_outer_contours(binarized_array)
        new_array = np.zeros_like(binarized_array)
        center = self.get_center(binarized_array)
    
        for contour in outer_contours:
            fat_outer_contour = self.make_contour_fat(contour, thickness, center, binarized_array)
            self.draw_fat_contour(new_array, fat_outer_contour, thickness, 1)
        
        return new_array
