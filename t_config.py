
class t_preproc_config(object): # preprocessing setup for Wavetrack ver. 0.40
    
    def __init__(self): 
       
       # Shows if base difference images to to be used.
       self.use_base_diff ='y' 
      
       # Differencing algorithm. Possible values are 'BASE_DIFF' 'RD', 'AWARE'
       
       # 1) 'BASE_DIFF' Base images are created by averaging series of images a few minutes prior
       # to the start of the eruption. Each time steps starts with base difference 
       # images which are constructed by subtracting base images from the current 
       # RAW image of the sequence.
       
       # 2) 'RD' Running difference or persistence running difference images (Ireland et al., 2019) 
       # are used when necessary to determine the moment when the eruption starts, or in case 
       # Wavetrack is used to study an active region or a filament.  Using running difference images 
       # introduces spurious features and is not recommended for discovering the true (projected) 
       # wave shape (Long et al., 2011)

       # 3) 'AWARE' The AWARE algorithm uses more advanced pre-processing  
       # persistence running  difference  images, to characterize the wave shapes  
       # along similar flare-centered sectors
       self.diff_algorithm = 'BASE_DIFF' 
       
       # Initial imags are resized to x_size y_size specified below 
       self.x_size=1024
       self.y_size=1024
       
       # Solar disk parameters 
       # Coordinates and center of the solar disk is taken from disk_radius, disk_center_x, 
       # disk_center_y in case they are not red automatically from metadata. For such configuration
       # the parameter  use_fixed_sun_radius='n' should be set to 'n' (No)
       self.use_fixed_sun_radius='n' 
       self.disk_radius=200 
       self.disk_center_x=512
       self.disk_center_y=512
       
       # Use manual sun radius. To be red from metadata otherwise
       self.use_manual_rsun='n' 
       self.manual_rsun=55
  
       # Input data type. 'FITS' or 'raster'
       self.input_data_format='FITS' 
       # Input data level. Could be 'AIA_1.5', 'AIA_1', AIA_1_resized_log (experimental)
       self.data_lev='OTHER'
       
       # Load png images instead 
       self.use_preprocessed_png = 'n' # Testing Use at your own risk. 
       
       self.fits_extension = '.fts'

       # Threshold for absolute values while initial file read
       self.low_threshold=-10000
       self.high_threshold=40000
       # Threshold for base difference files
       self.low_threshold_bd= -10000
       self.high_threshold_bd= 40000

       # Invert data during initial file read 
       self.data_invert_img_load='n'
       # Invert data after differencing 
       self.data_invert_pipeline='n'
      
       # Background image is processed in a parallel "thread"/object copy with separate pre- post- 
       # and decomposition parameters, and is used solely for better visual interpretation of the 
       # results, as a-trous wavelet method itself can work as an effective filtering technique 
       # See, the following parameters below:
       # self.bkg_resolution_bits,
       # self.bkg_thrhold_sigma,
       # self.bkg_grad_thrhold_sigma,
       # self.bkg_outside_thrhold_sigma,
       # self.bkg_scales_weights
       # 

       # Invert background images  
       self.data_invert_bkg='y'

       # 1) Use PIL library ("img") or numeric ("numeric") for image inversion for main image and 
       # second, background "thread" which is used for visual interpretation of the results 
       # 2) "numeric": numerical inversion of the array
       # Empirically, one way or another to be used depending on type of the input data
       self.data_invert_method='img' 
       self.data_invert_method_bkg='img'

       # Log initial image 
       self.adjust_dynrange='n'  
       self.dynrange_logbase=20

       # HERE ADD ON BACKGROUND IMAGES   

       # Load original (non differenced) data as background 
       self.bkg_load_original='y'
      
       # Image resize/rebin parameters
       self.resize='n'
       # Use manual rebin patch to adjust header metadata ? 
       self.manual_rebin_patch='n'
       # Use sunpy resample instead of manual fits/raster resize
       self.sunpy_resample='n'
       # Rebin factor. To be used together with rebin patch. 1024/4 = 256, for instance
       self.rebin_factor=4 
       
       # Input data with consequitive timesteps in FITS/raster format
       self.data_path='/data/'
       # FITS/raster data to create average base image. Usually 3-5 files 
       # a 2-5 minutes prior to the event 
       self.base_data_path='/base_data/'

       # Normalize data after Base/Running/Persistent(AWARE) differencing 
       self.normalize_init_data='y'
       # Circle mask to replace slar disk with zero pixels  
       self.apply_circle_mask='n'
       
       # Square mask to focus on a certain area of the image, ignoring the rest of the pixels
       # Useful when the region where the event takes place is known in advance
       self.x_1_sq = 100
       self.x_n_sq = 900
       self.y_1_sq = 0
       self.y_n_sq = 500

       self.apply_circle_mask_2 = 'n'
       self.circle_mask_2_timestep_1 = 8
       self.circle_mask_2_timestep_n = 50
       self.circle_mask_2_x_center = 336 # 716
       self.circle_mask_2_y_center = 546 # 530
       self.circle_mask_2_rad = 140 #90


       # Artificially increase sun radius multiplying by rsun_coeff. Could be useful sometimes on 
       # a very bright images
       self.rsun_coeff = 1.04 # 1.04

       self.km_per_x = 1800000 / (4096/self.x_size)  



class t_decompose_config(object): # decomposition setup

    
    def __init__(self):
        
        # An image bit depth determines the number of intensity levels to which an image could be 
        # theoretically resolved. Posterisation is the conversion of a continuous gradation of tone 
        # to several regions of fewer tones and happens naturally when image bit depth has been 
        # decreased.
       
        self.resolution_bits = 24

        # A-Trous Wavelet parameters. Amount of scales and weight coefficients to be used for image 
        # reconstruction 
        self.scales_n_max = 5
        self.scales_weights=[0,1,1,1,1,1,1,1,1]

   
        # Parameters for post-processing wavelet coefficients 
        
        # Additional PILLOW library parameters for PIL normalize and median filter 
        self.normalize='n'
        self.PIL_posterize='n' # Deprecated/not tested after v. 0.38. Use at your own risk.
        self.median_filt='n'   # Deprecated/not tested after v. 0.38. Use at your own risk   
        self.median_scale = 3  # Deprecated/not tested after v. 0.38. Use at your own risk
        self.PIL_posterize_ratio = 3 # Deprecated/not tested after ver 0.38. Use at your own risk
        
        # Work on scales, instead of wavelet coefficients
        self.use_scales_not_coeff='n' # Deprecated/not tested after ver 0.38. Use at your own risk

        # Parameters for feature recognition setup 
        # Gradient method 
        # The gradient field of the image is the result of applying the gradient operator to an image. 
        # In this case, the gradient field of the final object image (after the object mask is applied 
        # to the original image) is used for visual interpretation but can also serve as additional criteria 
        # for more robust object segmentation, as it highlights object contours, allows to determine saddle point
        # locations, and to calculate gradient vectors. Image gradients can also be used as part of training sets
        # for deep convolutional networks. By default, we use the Sobel-Feldman operator, a discrete differentiation 
        # operator, to compute an approximation of the gradient of the image intensity function.  At each pixel of 
        # the image, the result would be either the corresponding gradient vector, or the norm of this vector. 
        #  
        # kernel_sobel_gx= [[1,0,-1],
        #                  [2,0,-2],
        #                  [1,0,-1]]

        # kernel_sobel_gy= [[1,2,1],
        #                  [0,0,0],
        #                  [-1,-2,-1]]
   
        # Alternatively, a simple gradient could be applied (grad_method=Simple_grad ) 
        # self.kernel_grad = [[1,0,-1],
        #                    [0,0,0],
        #                    [-1,0,1]], 
        #
        # or curvature (grad_method=Hagenaer in each pixel point could be calculated according to 
        # Hagenaer et. al. 1999 Astr.J. 511:932-944,1999   
         
    
        self.grad_method='Sobel-Feldman'

        # Relative thresholding (statistics) 
        # Due to the significantly different statistical distributions
        # of pixel intensities, on-disk and off-limb parts of images are 
        # processed separately (apply_thrhold_circle_mask = 'y')
        # and the results are merged afterwards. 
        self.apply_thrhold_circle_mask = 'y'    

        # Threshold parameters for the limb (outside_thrhold_sigma), disk (thrhold_sigma) and 
        # gradient field (grad_thrhold_sigma). Sigma (standard deviation) units
        self.thrhold_sigma = 3.2
        self.grad_thrhold_sigma = 3.2
        self.outside_thrhold_sigma = 3.2 # 3.8
        # Minimum structure size in pixels. Smaller structures would be ommited 
        self.min_struct_size=150
         
        # Check which structures overlap within neighboring timesteps 
        self.search_overlap='y'
        
        # Split into intensity levels manually ( usually right setup of absolute thresholding )
        self.split_intensities = 'n'
        
        # Since each of the wavelet decomposition levels effectively represents its own 
        # image detail level separately, one of the higher decomposition levels (in practice fifth or sixth)
        # would reveal shapes of relatively large objects, almost completely omitting small scale
        # details and noise. After proper thresholding a binarised mask of a high decomposition scale is
        # applied to the joint structures mask as an additional filter. 
        # One of the higher levels of wavelet decomposition of the solar image is taken 
        # as a "reference mask", limiting the area where objects of interest are located 
        # by the shape of that mask.     
        self.reference_scale_n = 2  
        # Use coefficient (or scale otherwise)
        self.coeff_as_reference ='y'    

        # Segmentation of the image is done by iteratively splitting it into quadrants while each 
        # object is renumbered in accordance with the quadrant number it belongs to at each iteration, 
        # provided the object does not cross the border of the quadrant. 
        # The value of the corresponding gradient field (and thus, a possible contour location) serves
        # as an additional criterion for object selection. Initial estimation of the objects count 
        # and quadrant locations is done via a Monte-Carlo algorithm. Alternatively, one-pass ABC mask 
        # could be used (experimental, not recommended) 
        self.use_abc_mask = 'n' 
        
        # Verbose output mode. Save all intermidiate steps, decompositions and structures output_log 
        # folder. Used mainly for testing purposes 
        self.verbose_output = 'y'
        self.save_scales = 'y'
        
        # The meaning of the following parameters are identical to main, non-background image
        # decomposition parameters 

        self.bkg_resolution_bits = 24
        self.bkg_thrhold_sigma=0.0
        self.bkg_grad_thrhold_sigma= 0.0
        self.bkg_outside_thrhold_sigma=0.0
        self.bkg_scales_weights=[0,1,1,1,1,1,1,1,0,0]
            
        # Output folders (within current folder)
        self.struct_save_path = '/output_structs/'
        self.objects_mla_save_path = '/output_objects_mla/'
        self.log_save_path = '/output_log/'
        self.grad_save_path = '/output_gradients/'
        self.results_coord_fname = '/output_structs/strucures_coord.txt'
        self.results_overlap_fname = '/output_structs/strucures_overlap.txt'

        self.velocities_fname = '/velocities/velocities.csv'
        self.velocities_front_fname = '/velocities/velocities_front.csv'
        self.velocities_fname_km_s = '/velocities/velocities_km_s.csv'
        self.velocities_front_fname_km_s = '/velocities/velocities_front_km_s.csv'
      
        self.trainset_save_path = '/trainset/'
        self.visualize_save_path = '/visualize/'
        self.velocities_save_path = '/velocities/'
        
        # Window filter parameters. A final one-pass filter which eliminates small spurious 
        # details/noize
        self.window_filt_boxsize=50
        self.window_filt_increment=4
        self.window_filt_thrhold=0

        # Front thickness
        self.front_thikness = 40

        # Velocity calculation 
        self.calc_lucas_kanade = 'True' 
        self.calc_horn_schunk = 'True'
        self.calc_farneback = 'True'
        self.calc_flct = 'True'   
        self.calc_mass_cent_vl = 'True'        
        self.lucas_window_size = 5
        self.hs_alpha = 0.5 
        self.hs_num_itterations = 200
        self.flct_window_size = 30
          
        # Velocity calculation for the front      
        self.calc_lucas_kanade_front = 'True'
        self.calc_horn_schunk_front = 'True'
        self.calc_farneback_front = 'True'
        self.calc_flct_front = 'True'
        self.calc_mass_cent_vl_front = 'True'     
        self.lucas_window_size_front = 5
        self.hs_alpha_front = 0.2 
        self.hs_num_itterations_front = 200
        self.flct_window_size_front = 30

        self.velocity_x_size = 256
        self.velocity_y_size = 256 
        self.velocity_gauss_sigma = 3# was 5, when lk was zeros
        self.velocity_gauss_sigma_pp = 1.5
        self.velocity_gauss_sigma_flct = 3
        self.velocity_gauss_sigma_flct_2 = 5
        # The resolution of AIA images is about 0.6 arcseconds per pixel.
         
        '''
        AIA 

           Given that the Sun's average radius is about 696,340696,340696,340 km, 
           and considering that it subtends an angle of about 0.53 degrees 
           (approx 31.731.731.7 arcmin or 1,9021,9021,902 arcsec) from the Earth's surface, 
           linear distance per arcsecond as 731.69 km/arcsecond

           For the AIA's resolution:
           439.01 km/pixel

           Along each axis: 1,798,681.96 km


        LASCO C2:

           The LASCO C2 coronagraph has a field of view ranging from 1.5 to 6 solar radii.
           So, the distance covered from the center of the Sun is from 1,044,510 km (1.5 solar radii) 
           to 4,178,040 km (6 solar radii). The actual imaging area (or the diameter of the coronal 
           region observed) is 4,178,040−1,044,510=3,133,5304,178,040 - 1,044,510 = 3,133,5304,178,040−1,044,510=3,133,530 km.
           This doesn't directly provide the resolution in km/pixel without additional information,
           but it gives the full span of the instrument's field of view.
 
        LASCO C3:

            The LASCO C3 coronagraph has a field of view ranging from 3.7 to 30 solar radii. 
            So, the distance covered from the center of the Sun is from 2,577,658 km (3.7 solar radii) 
            to 20,890,200 km (30 solar radii). 
            The actual imaging area observed is 20,890,200−2,577,658=18,312,54220,890,200 - 2,577,658 = 18,312,54220,890,200−2,577,658=18,312,542 km in diameter.
        '''

        self.inst_x_size = 1024 # AIA = 4096
        self.inst_y_size = 1024 # AIA = 4096
    
        self.inst_per_x_pix = 8549600 # AIA = 1798000   
        self.inst_per_y_pix = 8549600 # AIA = 1790000 
        self.sec_per_frame = 12*60
        
        self.vlct_multipl_constant = ( (self.inst_per_x_pix/self.inst_x_size)* (self.inst_x_size/self.velocity_x_size) )/self.sec_per_frame

        # Internal/debugging parameters. Not to be modified 
        self.override_w_bkg = 'n'
        self.override_bkg_w_orig = 'n'
        self.use_scales_not_coeff='n'
        self.optimal_w_coeff = 3
        self.objects_gradients = 'n'
        self.use_imsave='y'
        self.dist_per_pixel=1
