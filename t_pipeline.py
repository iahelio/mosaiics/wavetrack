import t_mask
import t_math
import t_config
import os
import numpy as np
import copy
from scipy.stats import norm
import glob
from sunpy.net import Fido, attrs as a
from astropy import units as u
import misc

# Main pipeline which incapsulates feature recognition routines on multiple files
class t_pipeline(object):
   

    def download_files(self, begin_time, end_time, wavelength, cadence, files_path):
        
        '''
        Add instrument choice to the next version
        print('Making LASCO request...\n') 
        timerange = a.Time(begin_time, end_time)
        instrument = a.Instrument.lasco
        detector = a.Detector.c2
        search_result = Fido.search(timerange, instrument, detector)
        download_files=Fido.fetch(search_result,path=files_path+'/download/{file}')
        '''

        print('Making AIA request...\n') 
        search_result = Fido.search(a.Time(begin_time, end_time), a.Instrument.aia, a.Wavelength(wavelength*u.angstrom),a.vso.Sample(12*u.second))
        download_files=Fido.fetch(search_result,path=files_path+'/download/{file}')
        
        

        
    def base_diff_series(self, path, preproc_config ):

        files_arr = os.listdir(path)
        files_arr.sort()
        
        fname=path+files_arr[0]
        mask_base = t_mask.t_mask() 
        a_math = t_math.t_math() 
        b_math = t_math.t_math()
        mask_base.preproc_config=copy.deepcopy(preproc_config)

        print('Calculating base difference image...')

        for file_i in range(0,len(files_arr)):
                                    
            fname=path+files_arr[file_i]
            print('Processing file:'+fname)
            
            if (file_i==0):
                mask_read_sun_param = t_mask.t_mask()
                mask_read_sun_param.preproc_config=copy.deepcopy(preproc_config)
                mask_read_sun_param.preproc_config.apply_circle_mask='n'
                mask_read_sun_param.read_input_file(fname)
                
                self.square_mask= a_math.square_mask(preproc_config.x_1_sq, preproc_config.x_n_sq, preproc_config.y_1_sq, preproc_config.y_n_sq,preproc_config.x_size,preproc_config.y_size)
             

                if (preproc_config.use_fixed_sun_radius=='y'):
                    self.circle_mask= a_math.circle_mask(preproc_config.x_size, preproc_config.y_size, preproc_config.x_size/2, preproc_config.y_size/2, preproc_config.disk_radius)                
               
                else:                   
                    self.circle_mask= a_math.circle_mask(preproc_config.x_size, preproc_config.y_size, mask_read_sun_param.preproc_config.disk_center_x, mask_read_sun_param.preproc_config.disk_center_y, mask_read_sun_param.preproc_config.disk_radius)


            mask_i = t_mask.t_mask() 
            mask_i.preproc_config=copy.deepcopy(preproc_config)
            mask_i.circle_mask=self.circle_mask
            mask_i.square_mask=self.square_mask

            mask_i.timestep_i=file_i
            mask_i.read_input_file(fname)
            
            mask_i_arr = np.array(mask_i.img_init_data)
           
            if (file_i==0):
                mask_base_arr=np.copy(mask_i_arr)
            else:
                mask_base_arr=np.add(mask_base_arr, mask_i_arr)
            
            del mask_i 
        

        mask_base_arr=np.divide(mask_base_arr, len(files_arr))
        mask_base.img_init_data=np.array(mask_base_arr)
        return mask_base

            

    def find_objects_mla_uni(self, mask_obj,prev_mask_obj, mask_base_obj):
      
        a_mask = t_mask.t_mask()


        
        a_mask.preproc_config=copy.deepcopy(mask_obj.preproc_config)
        a_mask.decompose_config=copy.deepcopy(mask_obj.decompose_config) 
        a_mask.circle_mask= mask_obj.circle_mask #self.circle_mask
        a_mask.square_mask= mask_obj.square_mask
        a_mask.timestamp = mask_obj.timestamp
        a_mask.datetime_str = mask_obj.datetime_str
        a_mask.wavelength = mask_obj.wavelength
        a_mask.timestep_i =  mask_obj.timestep_i   
        a_mask.img_init_data_orig = list(mask_obj.img_init_data_orig)

        a_math = t_math.t_math()
        b_math = t_math.t_math()
    
        img_init_data_arr = np.array(mask_obj.img_init_data)
        img_init_data_arr_prev = np.array(prev_mask_obj.img_init_data) 
        
        if (a_mask.preproc_config.use_base_diff=='y'): 
                  
            print('Calculating base difference image...')
            base_img_init_data_arr = np.array(mask_base_obj.img_init_data) 
            img_init_data_arr=np.subtract(img_init_data_arr, base_img_init_data_arr)           


        if (a_mask.preproc_config.diff_algorithm=='AWARE'):

            print('Performing AWARE algorithm on running differences ')   
            img_init_data_arr = (img_init_data_arr>img_init_data_arr_prev)*img_init_data_arr 
            

        elif (a_mask.preproc_config.diff_algorithm=='RD'):

            print('Calculating Running Difference image')
            img_init_data_arr = np.subtract(img_init_data_arr,img_init_data_arr_prev)#*img_init_data_arr 
    

        img_init_low_trunc = (img_init_data_arr>mask_obj.preproc_config.low_threshold_bd)*img_init_data_arr
        img_init_trunc = (img_init_low_trunc<mask_obj.preproc_config.high_threshold_bd)*img_init_low_trunc
        img_init_trunc_32=a_math.bring_to_int32(img_init_trunc)
        
        if (a_mask.preproc_config.data_invert_pipeline=='y'):
         
            print('Inverting the data')

            if (a_mask.preproc_config.data_invert_method=='numeric'):
                img_init_trunc_32=a_math.invert_data_numeric(img_init_trunc_32)
            elif (a_mask.preproc_config.data_invert_method=='img'):
                img_init_trunc_32=a_math.invert_data_img(img_init_trunc_32) 
          
        
        if (mask_obj.preproc_config.normalize_init_data=='y'): 
           img_init_trunc_32_norm = a_math.normalize_posterize(img_init_trunc_32, mask_obj.decompose_config.resolution_bits)
           a_mask.img_init_data=list(img_init_trunc_32_norm)

        else: 
           a_mask.img_init_data=list(img_init_trunc_32)
         

        a_mask.calc_scales() 
        a_mask.scales_post_process()

        if (mask_obj.decompose_config.verbose_output=='y'):
            a_mask.save_scales(os.getcwd()+mask_obj.decompose_config.log_save_path) 
         
        # Another opject instance to apply reference masks and gradients on wavelet 
        # decompositions 

        work_mask = t_mask.t_mask()

        work_mask.preproc_config=copy.deepcopy(mask_obj.preproc_config)
        work_mask.decompose_config=copy.deepcopy(mask_obj.decompose_config)
        work_mask.header_orig=copy.deepcopy(mask_obj.header_orig)
        work_mask.circle_mask=mask_obj.circle_mask
        work_mask.square_mask=mask_obj.square_mask
        work_mask.timestamp = mask_obj.timestamp
        work_mask.datetime_str = mask_obj.datetime_str
        work_mask.wavelength = mask_obj.wavelength
        work_mask.timestep_i =  mask_obj.timestep_i 

        work_mask.decompose_config.scales_n_max=3
      
        work_mask.img_init_data=list(a_mask.img_init_data)
        work_mask.img_init_data_orig = list(a_mask.img_init_data_orig)
        work_mask.reference_mask = list(a_mask.reference_mask)
    
        work_mask.w_coeffs.append(a_mask.w_coeffs[0])
        work_mask.w_coeffs.append(a_mask.w_coeffs[a_mask.decompose_config.scales_n_max-2])
        work_mask.w_coeffs.append(a_mask.w_coeffs[a_mask.decompose_config.scales_n_max-1])
        
        work_mask.scales.append(a_mask.scales[0])
        work_mask.scales.append(a_mask.scales[a_mask.decompose_config.scales_n_max-2])
        work_mask.scales.append(a_mask.scales[a_mask.decompose_config.scales_n_max-1])

        work_mask.scales_post_process()
        
        bkg_mask = t_mask.t_mask()        
        bkg_mask.preproc_config=copy.deepcopy(mask_obj.preproc_config)
        bkg_mask.decompose_config=copy.deepcopy(mask_obj.decompose_config)
        
        bkg_mask.circle_mask=mask_obj.circle_mask
        bkg_mask.square_mask=mask_obj.square_mask
        bkg_mask.timestamp = mask_obj.timestamp
        bkg_mask.datetime_str = mask_obj.datetime_str
        bkg_mask.wavelength = mask_obj.wavelength
        bkg_mask.timestep_i =  mask_obj.timestep_i 
          
        bkg_mask.img_init_data= list(a_mask.img_init_data) # list(a_mask.img_init_data_orig)
        bkg_mask.img_init_data_orig = list(a_mask.img_init_data_orig)
        bkg_mask.reference_mask = list(a_mask.reference_mask)

        if (mask_obj.preproc_config.data_invert_bkg == 'y'): 
               
            print('Inverting the background data')

            if (a_mask.preproc_config.data_invert_method_bkg=='numeric'):
                bkg_mask.img_init_data=a_math.invert_data_numeric(bkg_mask.img_init_data)
            elif (a_mask.preproc_config.data_invert_method_bkg=='img'):
                bkg_mask.img_init_data=a_math.invert_data_img(np.array(bkg_mask.img_init_data)) 
       

        bkg_mask.decompose_config.resolution_bits = mask_obj.decompose_config.bkg_resolution_bits
        bkg_mask.decompose_config.thrhold_sigma= mask_obj.decompose_config.bkg_thrhold_sigma
        bkg_mask.decompose_config.grad_thrhold_sigma= mask_obj.decompose_config.bkg_grad_thrhold_sigma
        bkg_mask.decompose_config.outside_thrhold_sigma=mask_obj.decompose_config.bkg_outside_thrhold_sigma
        bkg_mask.decompose_config.scales_weights=copy.deepcopy(mask_obj.decompose_config.bkg_scales_weights)  
        bkg_mask.circle_mask= copy.deepcopy(mask_obj.circle_mask)

        work_mask.square_mask=copy.deepcopy(mask_obj.square_mask)

        bkg_mask.timestamp = mask_obj.timestamp
        bkg_mask.datetime_str = mask_obj.datetime_str
        bkg_mask.wavelength = mask_obj.wavelength
        bkg_mask.timestep_i =  mask_obj.timestep_i 
       
        bkg_mask.header_orig=copy.deepcopy(mask_obj.header_orig)
        bkg_mask.header_orig['DATAMIN']=np.amin(bkg_mask.img_init_data) 
        bkg_mask.header_orig['DATAMAX']=np.amax(bkg_mask.img_init_data)

        print('Preparing background image...')
        bkg_mask.calc_scales()
        bkg_mask.scales_post_process()
        
        if (mask_obj.decompose_config.override_w_bkg=='y'): 
            work_mask = copy.deepcopy(bkg_mask)
            work_mask.w_coeffs.append(bkg_mask.w_coeffs[0])
            work_mask.w_coeffs.append(bkg_mask.w_coeffs[a_mask.decompose_config.scales_n_max-2])
            work_mask.w_coeffs.append(bkg_mask.w_coeffs[a_mask.decompose_config.scales_n_max-1])
        
            work_mask.scales.append(bkg_mask.scales[0])
            work_mask.scales.append(bkg_mask.scales[a_mask.decompose_config.scales_n_max-2])
            work_mask.scales.append(bkg_mask.scales[a_mask.decompose_config.scales_n_max-1])
        
        a_mask.img_bkg_data=list(bkg_mask.decompositions[bkg_mask.decompose_config.scales_n_max-2])  
        
        if (work_mask.decompose_config.verbose_output=='y'):
            work_mask.save_decompositions(os.getcwd()+work_mask.decompose_config.log_save_path)


        if work_mask.decompose_config.grad_method=='Simple_grad': 
            work_mask.decompositions_simple_grad()

        if work_mask.decompose_config.grad_method=='Sobel-Feldman':       
            work_mask.decompositions_sobel_feldman()
        
        if work_mask.decompose_config.grad_method=='Hagenaer': 
            work_mask.decompositions_hagenaer()

        work_mask.apply_reference_mask_grads()

        work_mask.save_decomposition_grads(os.getcwd()+work_mask.decompose_config.grad_save_path) 
           
        work_mask.structures_masks(os.getcwd()+work_mask.decompose_config.struct_save_path,work_mask.decompositions_raw[2],work_mask.decompositions[2],work_mask.timestep_i,0,work_mask.decompose_config.min_struct_size)
        work_mask.apply_combined_structs_mask()  

        
        use_timestep = 0 

        if (work_mask.preproc_config.diff_algorithm=='RD'):        
            if ( (work_mask.timestep_i>2) and (prev_mask_obj.timestep_i>1)):
                work_mask.calc_velocities(prev_mask_obj)
                use_timestep = 1
        else:
            if ( (work_mask.timestep_i>1) and (prev_mask_obj.timestep_i>0)):
                work_mask.calc_velocities(prev_mask_obj)
                use_timestep = 1 
        
        
        #masks_cnt=b_math.outer_contour_only((np.array(work_mask.structs_combined_mask)), work_mask.decompose_config)
        

        print('Putting strucures and gradients as layers above the initial data')   
        #init_img_data_orig_int_32= a_math.bring_to_int32(np.array(work_mask.img_init_data_orig))
        img_data_bkg_int_32= a_math.bring_to_int32(np.array(bkg_mask.decompositions[bkg_mask.decompose_config.scales_n_max-2]))
        
        img_init_data_orig_int_32 = list(work_mask.img_init_data_orig)
        
        #AWARE/Running diff/base_diff depending on the configuration 
        img_init_data_int_32 = list(work_mask.img_init_data)
        
        #for training sets: 
        img_init_data_orig_w_structs_arr=work_mask.bind_2_layers_region(img_init_data_orig_int_32,work_mask.decompositions_raw[2])
        img_init_data_orig_w_grads_arr= work_mask.bind_2_layers_region(img_init_data_orig_int_32,work_mask.decompositions_grads_ref[2])
       
        #for visualizations: 
        bkg_data_w_structs_arr=work_mask.bind_2_layers_XOR(img_data_bkg_int_32,work_mask.decompositions_raw[2])
        bkg_data_w_grads_arr= work_mask.bind_2_layers_XOR(img_data_bkg_int_32,work_mask.decompositions_grads_ref[2])

        #for training sets, AWARE/RD/BASE_DIFF: 
        img_init_data_w_structs_arr=work_mask.bind_2_layers_XOR(img_init_data_int_32,work_mask.decompositions_raw[2])
        img_init_data_w_grads_arr= work_mask.bind_2_layers_XOR(img_init_data_int_32,work_mask.decompositions_grads_ref[2])
        
        diff_masks_fname_part='masks_projected_'+work_mask.preproc_config.diff_algorithm+'_data'
        diff_masks_fname_part_grad='masks_grads_projected_'+work_mask.preproc_config.diff_algorithm+'_data'
        diff_masks_fname_part_appl='masks_applied_'+work_mask.preproc_config.diff_algorithm+'_data'
        diff_fname_part=work_mask.preproc_config.diff_algorithm+'_data'
    


        use_frame=1
    
        if ( mask_obj.preproc_config.data_lev.find('AIA')>=0):
            if (mask_obj.header_orig['AECTYPE']!=0): 
                use_frame=0

        if (use_frame==1):
         
            combined_masks_arr=np.array(work_mask.structs_combined_mask)
            combined_masks_raw_arr=np.array(work_mask.structs_combined_raw)
            grads_masks_arr=np.array(work_mask.decompositions_grads_ref[2])
            
      
            fname_masks_grads_bkg_bw_projected=work_mask.get_save_fname(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw_w_grads_projected_bkg_2', np.array(img_data_bkg_int_32), work_mask.timestep_i,0)+'.png'       
            fname_masks_grads_bkg_bw_projected_2=work_mask.get_save_fname(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw_w_grads_projected_bkg', np.array(img_data_bkg_int_32), work_mask.timestep_i,0)+'.png'
            fname_masks_grads_bkg_bw_projected_2=work_mask.get_save_fname(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw_rnb_projected_bkg', np.array(img_data_bkg_int_32), work_mask.timestep_i,0)+'.png'        

            fname_bkg_bw=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'bkg_bw', np.array(img_data_bkg_int_32), work_mask.timestep_i,0, 'n')       
          
            fname_masks_raw=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw', combined_masks_raw_arr, work_mask.timestep_i,0, 'y') 
            fname_masks=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks', combined_masks_arr, work_mask.timestep_i,0, 'y')
            fname_masks_grads=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_grads', grads_masks_arr, work_mask.timestep_i,0, 'y')
        

            raw_plus_grads_masks_arr=work_mask.bind_2_layers_XOR(combined_masks_raw_arr, grads_masks_arr)
            
            fname_raw_w_grads=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw_w_grads', raw_plus_grads_masks_arr, work_mask.timestep_i,0, 'y')
        
            #fname_raw_w_grads_2=work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw_w_grads_transp', raw_plus_grads_masks_arr, work_mask.timestep_i,0, 'y')
    
            ''' 
             
            fname_masks_grads_bkg_bw_projected=fname_bkg_bw.replace("bkg_bw", "masks_raw_w_grads_projected_bkg_2", 1)      
            fname_masks_grads_bkg_bw_projected_2=fname_bkg_bw.replace("bkg_bw", "masks_raw_w_grads_projected_bkg", 1) 
            fname_masks_raw_rnb_bkg_bw_projected=fname_bkg_bw.replace("bkg_bw", "masks_raw_rnb_projected_bkg", 1) 
         
            '''

            a_math.merge_two_layers_transp_r_save_png( np.array(img_data_bkg_int_32), raw_plus_grads_masks_arr, fname_masks_grads_bkg_bw_projected) 
            
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_projected_bkg_bw', bkg_data_w_structs_arr, work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_grads_projected_bkg_bw', bkg_data_w_grads_arr, work_mask.timestep_i,0, 'n')
       
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part, img_init_data_w_structs_arr, work_mask.timestep_i,0, 'y')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part_grad, img_init_data_w_grads_arr, work_mask.timestep_i,0, 'y')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part+'_bw', img_init_data_w_structs_arr, work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part_grad+'_bw', img_init_data_w_grads_arr, work_mask.timestep_i,0, 'n')
            
            fname_masks_raw_fits=work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_raw_data', np.array(work_mask.structs_combined_raw), work_mask.timestep_i,0, mask_obj.header_orig)
                     
            if (work_mask.decompose_config.override_bkg_w_orig=='n'):
                fname_bkg_fits=work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'bkg', np.array(img_data_bkg_int_32), work_mask.timestep_i,0, bkg_mask.header_orig)
            else: 
                fname_bkg_fits=work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'bkg', np.array(img_init_data_orig_int_32), work_mask.timestep_i,0, bkg_mask.header_orig)
           

            fname_masks_raw_rnb_bkg_bw_projected=fname_bkg_fits.replace("bkg", "masks_raw_rnb_projected_bkg", 1) 
            fname_masks_raw_rnb_bkg_bw_projected=fname_masks_raw_rnb_bkg_bw_projected.replace("fits", "png", 1) 
           

            a_math.merge_two_fits(fname_bkg_fits,fname_masks_raw_fits, fname_masks_raw_rnb_bkg_bw_projected)
            
            work_mask.header_orig['DATAMIN']=np.amin(work_mask.img_init_data) 
            work_mask.header_orig['DATAMAX']=np.amax(work_mask.img_init_data)

            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks', np.array(work_mask.structs_combined_mask), work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, 'masks_grads', np.array(grads_masks_arr), work_mask.timestep_i,0, work_mask.header_orig)
        
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part, img_init_data_w_structs_arr, work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.visualize_save_path, diff_masks_fname_part_grad, img_init_data_w_grads_arr, work_mask.timestep_i,0, work_mask.header_orig)
     
            # from version 42 using  work_mask.structs_front_mask= [],  self.structs_front_raw= []                           
            #front_mask=b_math.outer_contour_only((np.array(work_mask.structs_combined_mask)), work_mask.decompose_config)
         

            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks_applied_orig_data', img_init_data_orig_w_structs_arr, work_mask.timestep_i,0, 'y')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks', np.array(work_mask.structs_combined_mask), work_mask.timestep_i,0, 'n')
            # from version 
            #work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks_front', np.array(masks_cnt), work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks_front', work_mask.structs_front_mask_arr, work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks_front_raw', work_mask.structs_front_raw_arr, work_mask.timestep_i,0, 'n')
           

            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, diff_masks_fname_part_appl, np.array(work_mask.structs_combined_raw), work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'orig_data', img_init_data_orig_int_32, work_mask.timestep_i,0, 'n')
            work_mask.save_data(os.getcwd()+work_mask.decompose_config.trainset_save_path, diff_fname_part,img_init_data_int_32,work_mask.timestep_i,0, 'n')

            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks_applied_orig_data', img_init_data_orig_w_structs_arr, work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks', np.array(work_mask.structs_combined_mask), work_mask.timestep_i,0, work_mask.header_orig)
            #work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks_cnt', np.array(masks_cnt), work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks_front', work_mask.structs_front_mask_arr, work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'masks_front_raw', work_mask.structs_front_raw_arr, work_mask.timestep_i,0, work_mask.header_orig)
        

            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, diff_masks_fname_part_appl, np.array(work_mask.structs_combined_raw), work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, 'orig_data', img_init_data_orig_int_32, work_mask.timestep_i,0, work_mask.header_orig)
            work_mask.save_data_FITS(os.getcwd()+work_mask.decompose_config.trainset_save_path, diff_fname_part,img_init_data_int_32,work_mask.timestep_i,0, work_mask.header_orig)
            
        
           
            if (use_timestep == 1 ):
            #if ( (work_mask.timestep_i>1) and (prev_mask_obj.timestep_i>0)):
                '''
                and 
                ( (self.calc_lucas_kanade=='True') or  (self.calc_horn_schunck=='True') or 
                 (self.calc_flct == 'True') or (self.calc_mass_cent_vl == 'True') or 
                 (self.calc_lucas_kanade_front == 'True') or (self.calc_lucas_kanade_front = 'True') or 
                 (self.calc_horn_schunck_front == 'True') or (self.calc_flct_front = 'True') or
                 (self.calc_mass_cent_vl_front == 'True')) :
                
                work_mask.calc_velocities(prev_mask_obj)
                '''
                empty_arr= np.zeros_like(work_mask.img_init_data_orig) 
                self.velocities_n = self.velocities_n + 1
                self.datetime_strs.append(work_mask.datetime_str)

                if (self.decompose_config.calc_lucas_kanade_front=='True'): 
                    self.lk_front_n = self.lk_front_n+1 
                    self.vf_scalar_lk_front.append(work_mask.vf_scalar_lk_front)
                    self.dest_points_lk_front.append(work_mask.dest_points_lk_front)
                    self.avg_velocity_lk_front.append(work_mask.avg_velocity_lk_front)

                    this_vf_scalar_lk_front_max = np.amax(work_mask.vf_scalar_lk_front_arr)
                    
                    if (self.vf_scalar_lk_front_max < this_vf_scalar_lk_front_max):
                        self.vf_scalar_lk_front_max = this_vf_scalar_lk_front_max
                 
                    this_dest_points_lk_front_max = np.amax(work_mask.dest_points_lk_front_arr) 
                           
                    if (self.dest_points_lk_front_max < this_dest_points_lk_front_max):
                        self.dest_points_lk_front_max = this_dest_points_lk_front_max  
                

                else: 

                    self.vf_scalar_lk_front.append(empty_arr)
                    self.dest_points_lk_front.append(empty_arr)
                    self.avg_velocity_lk_front.append(empty_arr)

                
                if (self.decompose_config.calc_horn_schunk_front=='True'): 

                    self.hs_front_n = self.hs_front_n+1 
                    self.vf_scalar_hs_front.append(work_mask.vf_scalar_hs_front)
                    self.dest_points_hs_front.append(work_mask.dest_points_hs_front)
                    self.avg_velocity_hs_front.append(work_mask.avg_velocity_hs_front)
                 
                    this_vf_scalar_hs_front_max = np.amax(work_mask.vf_scalar_hs_front_arr)
                    if (self.vf_scalar_hs_front_max < this_vf_scalar_hs_front_max):
                        self.vf_scalar_hs_front_max = this_vf_scalar_hs_front_max
                 
                    this_dest_points_hs_front_max = np.amax(work_mask.dest_points_hs_front_arr) 
                    if (self.dest_points_hs_front_max  < this_dest_points_hs_front_max):
                        self.dest_points_hs_front_max = this_dest_points_hs_front_max            
               
                else: 
                    self.vf_scalar_hs_front.append(empty_arr)
                    self.dest_points_hs_front.append(empty_arr)
                    self.avg_velocity_hs_front.append(empty_arr)


                if (self.decompose_config.calc_farneback_front=='True'): 

                    self.fb_front_n = self.fb_front_n+1 
                    self.vf_scalar_fb_front.append(work_mask.vf_scalar_fb_front)
                    self.dest_points_fb_front.append(work_mask.dest_points_fb_front)
                    self.avg_velocity_fb_front.append(work_mask.avg_velocity_fb_front)
                 
                    this_vf_scalar_fb_front_max = np.amax(work_mask.vf_scalar_fb_front_arr)
                    if (self.vf_scalar_fb_front_max < this_vf_scalar_fb_front_max):
                        self.vf_scalar_fb_front_max = this_vf_scalar_fb_front_max
                 
                    this_dest_points_fb_front_max = np.amax(work_mask.dest_points_fb_front_arr) 
                    if (self.dest_points_fb_front_max  < this_dest_points_fb_front_max):
                        self.dest_points_fb_front_max = this_dest_points_fb_front_max                           
                else: 
                    self.vf_scalar_fb_front.append(empty_arr)
                    self.dest_points_fb_front.append(empty_arr)
                    self.avg_velocity_fb_front.append(empty_arr)



                if (self.decompose_config.calc_flct_front == 'True'): 
                    self.flct_front_n = self.flct_front_n+1 
                    self.vf_scalar_flct_front.append(work_mask.vf_scalar_flct_front)
                    self.dest_points_flct_front.append(work_mask.dest_points_flct_front)
                    self.avg_velocity_flct_front.append(work_mask.avg_velocity_flct_front)
                 
                    this_vf_scalar_flct_front_max = np.amax(work_mask.vf_scalar_flct_front_arr)
                    if (self.vf_scalar_flct_front_max < this_vf_scalar_flct_front_max):
                        self.vf_scalar_flct_front_max = this_vf_scalar_flct_front_max
                 
                    this_dest_points_flct_front_max = np.amax(work_mask.dest_points_flct_front_arr) 
                    if (self.dest_points_flct_front_max  < this_dest_points_flct_front_max):
                        self.dest_points_flct_front_max = this_dest_points_flct_front_max
                
                else: 
                    self.vf_scalar_flct_front.append(empty_arr)
                    self.dest_points_flct_front.append(empty_arr)
                    self.avg_velocity_flct_front.append(empty_arr)


                if (self.decompose_config.calc_lucas_kanade == 'True'): 
                    self.lk_n = self.lk_n+1 
                    self.vf_scalar_lk.append(work_mask.vf_scalar_lk)
                    self.dest_points_lk.append(work_mask.dest_points_lk)
                    self.avg_velocity_lk.append(work_mask.avg_velocity_lk)
                 
                    this_vf_scalar_lk_max = np.amax(work_mask.vf_scalar_lk_arr)
                    if (self.vf_scalar_lk_max < this_vf_scalar_lk_max):
                        self.vf_scalar_lk_max = this_vf_scalar_lk_max
                 
                    this_dest_points_lk_max = np.amax(work_mask.dest_points_lk_arr) 
                    if (self.dest_points_lk_max  < this_dest_points_lk_front_max):
                        self.dest_points_lk_max = this_dest_points_lk_max
            
                else: 
                    self.vf_scalar_lk.append(empty_arr)
                    self.dest_points_lk.append(empty_arr)
                    self.avg_velocity_lk.append(empty_arr)


                if (self.decompose_config.calc_horn_schunk == 'True'): 
                    self.hs_n = self.hs_n+1 
                    self.vf_scalar_hs.append(work_mask.vf_scalar_hs)
                    self.dest_points_hs.append(work_mask.dest_points_hs)
                    self.avg_velocity_hs.append(work_mask.avg_velocity_hs)
                 
                    this_vf_scalar_hs_max = np.amax(work_mask.vf_scalar_hs_arr)
                    if (self.vf_scalar_hs_max < this_vf_scalar_hs_max):
                        self.vf_scalar_hs_max = this_vf_scalar_hs_max
                 
                    this_dest_points_hs_max = np.amax(work_mask.dest_points_hs_arr) 
                    if (self.dest_points_hs_max  < this_dest_points_hs_max):
                        self.dest_points_hs_max = this_dest_points_hs_max
        
                else: 
                    self.vf_scalar_hs.append(empty_arr)
                    self.dest_points_hs.append(empty_arr)
                    self.avg_velocity_hs.append(empty_arr)


                if (self.decompose_config.calc_farneback == 'True'): 
                    self.fb_n = self.hs_n+1 
                    self.vf_scalar_fb.append(work_mask.vf_scalar_fb)
                    self.dest_points_fb.append(work_mask.dest_points_fb)
                    self.avg_velocity_fb.append(work_mask.avg_velocity_fb)
                 
                    this_vf_scalar_fb_max = np.amax(work_mask.vf_scalar_fb_arr)
                    if (self.vf_scalar_fb_max < this_vf_scalar_fb_max):
                        self.vf_scalar_fb_max = this_vf_scalar_fb_max
                 
                    this_dest_points_fb_max = np.amax(work_mask.dest_points_fb_arr) 
                    if (self.dest_points_fb_max  < this_dest_points_fb_max):
                        self.dest_points_fb_max = this_dest_points_fb_max
        
                else: 
                    self.vf_scalar_fb.append(empty_arr)
                    self.dest_points_fb.append(empty_arr)
                    self.avg_velocity_fb.append(empty_arr)

            
                if (self.decompose_config.calc_flct=='True'): 
                    self.flct_n = self.flct_n+1 
                    self.vf_scalar_flct.append(work_mask.vf_scalar_flct)
                    self.dest_points_flct.append(work_mask.dest_points_flct)
                    self.avg_velocity_flct.append(work_mask.avg_velocity_flct)
                 
                    this_vf_scalar_flct_max = np.amax(work_mask.vf_scalar_flct_arr)
                    if (self.vf_scalar_flct_max < this_vf_scalar_flct_max):
                        self.vf_scalar_flct_max = this_vf_scalar_flct_max
                 
                    this_dest_points_flct_max = np.amax(work_mask.dest_points_flct_arr) 
                    if (self.dest_points_flct_max  < this_dest_points_flct_max):
                        self.dest_points_flct_max = this_dest_points_flct_max

                else: 
                    self.vf_scalar_flct.append(empty_arr)
                    self.dest_points_flct.append(empty_arr)
                    self.avg_velocity_flct.append(empty_arr)
                
                   
                if (self.decompose_config.calc_mass_cent_vl_front=='True'): 
                    
                    self.avg_mass_cent_vl_front = self.avg_mass_cent_vl_front + work_mask.avg_velocity_mass_cent_front
                    self.avg_geom_cent_vl_front = self.avg_geom_cent_vl_front + work_mask.avg_velocity_geom_cent_front  
                    
                    self.avg_velocity_mass_cent_front.append(work_mask.avg_velocity_mass_cent_front)
                    self.avg_velocity_geom_cent_front.append(work_mask.avg_velocity_geom_cent_front)
                else:
          
                    self.avg_velocity_mass_cent_front.append(0)
                    self.avg_velocity_geom_cent_front.append(0)
        
                
                
                if (self.decompose_config.calc_mass_cent_vl=='True'): 
                     
                    self.avg_mass_cent_vl = self.avg_mass_cent_vl + work_mask.avg_velocity_mass_cent
                    self.avg_geom_cent_vl = self.avg_geom_cent_vl + work_mask.avg_velocity_geom_cent  
                    
                    self.avg_velocity_mass_cent.append(work_mask.avg_velocity_mass_cent)
                    self.avg_velocity_geom_cent.append(work_mask.avg_velocity_geom_cent)
                else:
          
                    self.avg_velocity_mass_cent.append(0)
                    self.avg_velocity_geom_cent.append(0)
          



        return copy.deepcopy(work_mask)



    def track_objects_mla_uni(self, mask_base, preproc_config, decompose_config):

        a_math=t_math.t_math()
        b_math=t_math.t_math()
       
        files_arr = os.listdir(os.getcwd()+preproc_config.data_path)
        files_arr.sort()
     
        results_coord_f = open(os.getcwd()+decompose_config.results_coord_fname, 'w')
        results_overlap_f = open(os.getcwd()+decompose_config.results_overlap_fname, 'w')
       
        velocities_f = open(os.getcwd()+decompose_config.velocities_fname, 'w') 
        velocities_front_f = open(os.getcwd()+decompose_config.velocities_front_fname, 'w') 
        
        velocities_f_km_s = open(os.getcwd()+decompose_config.velocities_fname_km_s, 'w') 
        velocities_front_f_km_s = open(os.getcwd()+decompose_config.velocities_front_fname_km_s, 'w')

        line='Lukas-Kanade, Horn-Schunk, Farneback, FLCT, Geom Ceter, Mass Center \n'; 
        line_km_s='Lukas-Kanade, Horn-Schunk, Farneback, FLCT, Geom Ceter, Mass Center \n'; 
        line_front = 'Lukas-Kanade, Horn-Schunk, Farneback, FLCT, Geom Ceter, Mass Center \n'; 
        line_front_km_s = 'Lukas-Kanade, Horn-Schunk, Farneback, FLCT, Geom Ceter, Mass Center \n'; 


        velocities_f.write(line)           
        velocities_front_f.write(line)

        velocities_f_km_s.write(line_km_s)           
        velocities_front_f_km_s.write(line_km_s)

        overlap_count = 0
     
        for file_i in range(0,len(files_arr)):

            fname=os.getcwd()+preproc_config.data_path+files_arr[file_i]
            print('Processing file:'+fname)
                   
            mask_i = t_mask.t_mask() 
            mask_i.preproc_config=copy.deepcopy(preproc_config)
            mask_i.decompose_config=copy.deepcopy(decompose_config)     
            mask_i.timestep_i=file_i
     
            #mask_i.circle_mask=copy.deepcopy(self.circle_mask)
            #mask_i.square_mask=copy.deepcopy(self.square_mask)
            #mask_i.read_input_file(fname)
                        
            if (file_i==0):
                mask_read_sun_param = t_mask.t_mask()
                mask_read_sun_param.preproc_config=copy.deepcopy(preproc_config)
                mask_read_sun_param.preproc_config.apply_circle_mask='n'
                mask_read_sun_param.read_input_file(fname)
                
                self.square_mask= a_math.square_mask(preproc_config.x_1_sq, preproc_config.x_n_sq, preproc_config.y_1_sq, preproc_config.y_n_sq,preproc_config.x_size,preproc_config.y_size)
             
                if (preproc_config.use_fixed_sun_radius=='y'):
                    self.circle_mask= a_math.circle_mask(preproc_config.x_size, preproc_config.y_size, preproc_config.x_size/2, preproc_config.y_size/2, preproc_config.disk_radius)                   
                else: 
                    self.circle_mask= a_math.circle_mask(preproc_config.x_size, preproc_config.y_size, mask_read_sun_param.preproc_config.disk_center_x, mask_read_sun_param.preproc_config.disk_center_y, mask_read_sun_param.preproc_config.disk_radius)
                

                mask_k = t_mask.t_mask() 
                mask_k=copy.deepcopy(mask_read_sun_param)
                del mask_read_sun_param 
            

            mask_i.circle_mask=copy.deepcopy(self.circle_mask)
            mask_i.square_mask=copy.deepcopy(self.square_mask)
            mask_i.read_input_file(fname)
       

            if (file_i==0):
               mask_i_m1=copy.deepcopy(mask_i)
            
            # Checking if the frame needs to be skipped due to AECTYPE parameter from metadata 
            # In case the source is AIA 

            use_frame=1 
    
            if ( preproc_config.data_lev.find('AIA')>=0):
               if (mask_i.header_orig['AECTYPE']!=0): 
                    use_frame=0


            if (use_frame==1):
                
                mask_i.timestep_i = file_i
                mask_i_m1.timestep_i = file_i - 1 

                mask_i_proc=self.find_objects_mla_uni(mask_i, mask_i_m1, mask_base) # mask object with structures 
                             
                # saving coordinates of separate structures 

                for struct_i in range (0, mask_i_proc.structs_n): 
                   # write as csv. 

                    line='Structure no:'+str(struct_i+1)+' at '+mask_i_proc.datetime_str+'\n'
                    print(line)
                    results_coord_f.write(line)

                    line='geom_x_cent: '+ str(mask_i_proc.structs_geom_x_cent[struct_i])+'\n'
                    print(line)
                    results_coord_f.write(line)
                
                    line='geom_y_cent: '+ str(mask_i_proc.structs_geom_y_cent[struct_i])+'\n'
                    print(line)
                    results_coord_f.write(line)

                    line='mass_x_cent: '+ str(mask_i_proc.structs_x_cent[struct_i])+'\n'
                    print(line)
                    results_coord_f.write(line)
                
                    line='mass_y_cent: '+ str(mask_i_proc.structs_y_cent[struct_i])+'\n'
                    print(line)
                    results_coord_f.write(line)
                    
                # looking for overlaping structures (deprecated since ver 0.35) 
                if ( (decompose_config.search_overlap=='y') and (file_i>1) ): 
                
                    for struct_i in range(0,mask_i_proc.structs_n): 
                        for struct_i_prev in range(0,mask_i_m1.structs_n):
                           
                             
                            do_overlap = mask_i_proc.masks_overlap(mask_i_proc.structs_mask[struct_i],mask_i_m1.structs_mask[struct_i_prev] )                  
                        
                            if (do_overlap==1): 
                                overlap_count=overlap_count+1

                                line='Strucutre overlaps with a structure at:'+mask_i_m1.datetime_str+'\n' 
                                     
                                print(line)
                                results_overlap_f.write(line) 

                                line='Structure no:'+str(struct_i_prev)+' at '+mask_i_m1.datetime_str+'\n'
                                print(line)
                                results_overlap_f.write(line)

                                line='geom_x_cent: '+ str(mask_i_m1.structs_geom_x_cent[struct_i_prev])+'\n'
                                print(line)
                                results_overlap_f.write(line)
                
                                line='geom_y_cent: '+ str(mask_i_m1.structs_geom_y_cent[struct_i_prev])+'\n'
                                print(line)
                                results_overlap_f.write(line)

                                line='mass_x_cent: '+ str(mask_i_m1.structs_x_cent[struct_i_prev])+'\n'
                                print(line)
                                results_overlap_f.write(line)
                
                                line='mass_y_cent: '+ str(mask_i_m1.structs_y_cent[struct_i_prev])+'\n'
                                print(line)
                                results_overlap_f.write(line)

                                #calc speed
                del mask_i_m1
         
                        
                print('Saving copy of current structures as previos...')            
                mask_i_m1=t_mask.t_mask()
                mask_i_m1=copy.deepcopy(mask_i_proc)
                mask_i_m1.img_init_data=list(mask_i.img_init_data)

                del mask_i_proc
                    

            del mask_i    


        #int32_max = np.iinfo(np.int32).max
        #int32_min = np.iinfo(np.int32).min
  
        
        '''
        self.vf_scalar_lk_front_norm=misc.normalize_3d_list(self.vf_scalar_lk_front, 0 , self.vf_scalar_lk_front_max)
        self.vf_scalar_hs_front_norm=misc.normalize_3d_list(self.vf_scalar_hs_front, 0 , self.vf_scalar_hs_front_max)
        self.vf_scalar_flct_front_norm=misc.normalize_3d_list(self.vf_scalar_flct_front, 0 , self.vf_scalar_flct_front_max)
        
        self.vf_scalar_lk_norm=misc.normalize_3d_list(self.vf_scalar_lk, 0 , self.vf_scalar_lk_max)
        self.vf_scalar_hs_norm=misc.normalize_3d_list(self.vf_scalar_hs, 0 , self.vf_scalar_hs_max)
        self.vf_scalar_flct_norm=misc.normalize_3d_list(self.vf_scalar_flct, 0 , self.vf_scalar_flct_max)
        '''
        
        self.vf_scalar_lk_front_norm=self.vf_scalar_lk_front
        self.vf_scalar_hs_front_norm=self.vf_scalar_hs_front
        self.vf_scalar_fb_front_norm=self.vf_scalar_hs_front
        self.vf_scalar_flct_front_norm=self.vf_scalar_flct_front
        
        self.vf_scalar_lk_norm=self.vf_scalar_lk
        self.vf_scalar_hs_norm=self.vf_scalar_hs
        self.vf_scalar_fb_norm=self.vf_scalar_hs
        self.vf_scalar_flct_norm=self.vf_scalar_flct
        

        self.avg_mass_cent_vl_front = (self.avg_mass_cent_vl_front/self.velocities_n)/(self.preproc_config.x_size/self.decompose_config.velocity_x_size)             
        self.avg_mass_cent_vl = (self.avg_mass_cent_vl/self.velocities_n)/(self.preproc_config.x_size/self.decompose_config.velocity_x_size)         

        self.avg_geom_cent_vl_front = (self.avg_geom_cent_vl_front/self.velocities_n)/(self.preproc_config.x_size/self.decompose_config.velocity_x_size)             
        self.avg_geom_cent_vl = (self.avg_geom_cent_vl/self.velocities_n)/(self.preproc_config.x_size/self.decompose_config.velocity_x_size)

        
        for velocities_i in range(0,self.velocities_n): 

            line=''
            line_front=''
            
            mask_k.datetime_str = self.datetime_strs[velocities_i]

            if (self.decompose_config.calc_lucas_kanade_front == 'True'): 

                
                vf_scalar_lk_front_i = np.copy(self.vf_scalar_lk_front_norm[velocities_i])
                dest_points_lk_front_i = np.copy(self.dest_points_lk_front[velocities_i])
                avg_velocity_lk_front_i = np.copy(self.avg_velocity_lk_front[velocities_i])
                
               
                mask_k.save_data_FITS(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_lk_front_pix',  vf_scalar_lk_front_i, velocities_i, 0, mask_k.header_orig)
                mask_k.save_data_norm(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_lk_front', vf_scalar_lk_front_i, velocities_i, 0, 'y')
                #mask_k.save_histogram(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_lk_front_hist_pix', vf_scalar_lk_front_i, velocities_i, 0 )       
                #mask_k.save_histogram(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_lk_front_hist', vf_scalar_lk_front_i, velocities_i, 0 )       
                
                
                avg_velocity_lk_front_str= str(avg_velocity_lk_front_i) 
                avg_velocity_lk_front_km_s_str= str(avg_velocity_lk_front_i*self.decompose_config.vlct_multipl_constant) 

                print('Average Lucas-Kanade front velocity for '+str(velocities_i)+' timestep: '+ avg_velocity_lk_front_str +'\n')            
           
            else: 
                avg_velocity_lk_front_str = 'NaN'       
                avg_velocity_lk_front_km_s_str = 'NaN' 
            

            line_front = line_front + avg_velocity_lk_front_str +', '
            line_front_km_s = line_front_km_s + avg_velocity_lk_front_km_s_str +', ' 
            
            if (self.decompose_config.calc_horn_schunk_front == 'True'): 
            

                vf_scalar_hs_front_i = np.copy(self.vf_scalar_hs_front_norm[velocities_i])
                dest_points_hs_front_i = np.copy(self.dest_points_hs_front[velocities_i])
                avg_velocity_hs_front_i = np.copy(self.avg_velocity_hs_front[velocities_i])

                mask_k.save_data_FITS(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_hs_front',  vf_scalar_hs_front_i, velocities_i, 0, mask_k.header_orig)
                mask_k.save_data_norm(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_hs_front', vf_scalar_hs_front_i, velocities_i, 0, 'y')
                #mask_k.save_histogram(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_hs_front_hist', vf_scalar_hs_front_i, velocities_i, 0)
                
                avg_velocity_hs_front_str = str(avg_velocity_hs_front_i)
                avg_velocity_hs_front_km_s_str= str(avg_velocity_hs_front_i*self.decompose_config.vlct_multipl_constant) 
 
                print('Average Horn-Schunk front velocity for '+str(velocities_i)+' timestep: '+avg_velocity_hs_front_str +'\n')            
           
            else: 
                avg_velocity_hs_front_str = 'NaN'       
                avg_velocity_hs_front_km_s_str = 'NaN' 

            line_front = line_front + avg_velocity_hs_front_str +', '
            line_front_km_s = line_front_km_s + avg_velocity_hs_front_km_s_str +', '
            

            if (self.decompose_config.calc_farneback_front == 'True'): 
            

                vf_scalar_fb_front_i = np.copy(self.vf_scalar_fb_front_norm[velocities_i])
                dest_points_fb_front_i = np.copy(self.dest_points_fb_front[velocities_i])
                avg_velocity_fb_front_i = np.copy(self.avg_velocity_fb_front[velocities_i])

                mask_k.save_data_FITS(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_fb_front',  vf_scalar_fb_front_i, velocities_i, 0, mask_k.header_orig)
                mask_k.save_data_norm(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_fb_front', vf_scalar_fb_front_i, velocities_i, 0, 'y')
                #mask_k.save_histogram(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_fb_front_hist', vf_scalar_fb_front_i, velocities_i, 0)
               
                avg_velocity_fb_front_str= str(avg_velocity_fb_front_i) 
                avg_velocity_fb_front_km_s_str= str(avg_velocity_fb_front_i*self.decompose_config.vlct_multipl_constant) 
 
                print('Average Farneback front velocity for '+str(velocities_i)+' timestep: '+avg_velocity_fb_front_str +'\n')            
           
            else: 
                avg_velocity_fb_front_str = 'NaN'
                avg_velocity_fb_front_km_s_str = 'NaN'    


            line_front = line_front + avg_velocity_fb_front_str +', '
            line_front_km_s = line_front_km_s + avg_velocity_fb_front_km_s_str +', '
                          
             
            if (self.decompose_config.calc_flct_front == 'True'): 
            

                vf_scalar_flct_front_i = np.copy(self.vf_scalar_flct_front_norm[velocities_i])
                dest_points_flct_front_i = np.copy(self.dest_points_flct_front[velocities_i])
                avg_velocity_flct_front_i = np.copy(self.avg_velocity_flct_front[velocities_i] ) 

                mask_k.save_data_FITS(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_flct_front',  vf_scalar_flct_front_i, velocities_i, 0, mask_k.header_orig)
                #here try other save data options, check for nan values
                mask_k.save_data_norm(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_flct_front_norm', vf_scalar_flct_front_i, velocities_i, 0, 'y')
                mask_k.save_data(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_flct_front', vf_scalar_flct_front_i, velocities_i, 0, 'y')
                #mask_k.save_histogram(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_flct_front_hist', vf_scalar_flct_front_i, velocities_i, 0)
                
                avg_velocity_flct_front_str= str(avg_velocity_flct_front_i) 
                avg_velocity_flct_front_km_s_str= str(avg_velocity_flct_front_i*self.decompose_config.vlct_multipl_constant) 
  

                print('Average FLCT front velocity for '+str(velocities_i)+' timestep: '+avg_velocity_flct_front_str +'\n')            
           

            else: 
                avg_velocity_flct_front_str = 'NaN'       
                avg_velocity_flct_front_km_s_str = 'NaN' 

            line_front = line_front + avg_velocity_flct_front_str +', '
            line_front_km_s = line_front_km_s + avg_velocity_flct_front_km_s_str +', '

            if (self.decompose_config.calc_mass_cent_vl_front == 'True'): 
             
               avg_velocity_geom_cent_front_str = str(self.avg_velocity_geom_cent_front[velocities_i]) 
               avg_velocity_mass_cent_front_str = str(self.avg_velocity_mass_cent_front[velocities_i]) 
               
               avg_velocity_geom_cent_front_km_s_str = str(self.avg_velocity_geom_cent_front[velocities_i]*self.decompose_config.vlct_multipl_constant)  
               avg_velocity_mass_cent_front_km_s_str = str(self.avg_velocity_mass_cent_front[velocities_i]*self.decompose_config.vlct_multipl_constant) 
                             
 
               print('Average geom. center  front velocity for '+str(velocities_i)+' timestep: '+avg_velocity_geom_cent_front_str +'\n')            
               print('Average mass  center  front velocity for '+str(velocities_i)+' timestep: '+avg_velocity_mass_cent_front_str +'\n')            
        
              
            else: 
            
                avg_velocity_geom_cent_front_str = 'NaN'
                avg_velocity_mass_cent_front_str = 'NaN'
                avg_velocity_geom_cent_front_km_s_str = 'NaN'
                avg_velocity_mass_cent_front_km_s_str = 'NaN' 
          

               
            line_front = line_front + avg_velocity_geom_cent_front_str + ', ' + avg_velocity_mass_cent_front_str+'\n' 
            line_front_km_s = line_front_km_s + avg_velocity_geom_cent_front_km_s_str + ', ' + avg_velocity_mass_cent_front_km_s_str+'\n' 
        


            if (self.decompose_config.calc_lucas_kanade == 'True'): 
                
                vf_scalar_lk_i = np.copy(self.vf_scalar_lk_norm[velocities_i])
                dest_points_lk_i = np.copy(self.dest_points_lk[velocities_i])
                avg_velocity_lk_i = np.copy(self.avg_velocity_lk[velocities_i]) 

                mask_k.save_data_FITS(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_lk',  vf_scalar_lk_i, velocities_i, 0, mask_k.header_orig)
                mask_k.save_data_norm(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_lk', vf_scalar_lk_i, velocities_i, 0, 'y')
                #mask_k.save_histogram(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_lk_hist', vf_scalar_lk_i, velocities_i, 0)
               
                avg_velocity_lk_str = str(avg_velocity_lk_i) 
                avg_velocity_lk_km_s_str= str(avg_velocity_lk_i*self.decompose_config.vlct_multipl_constant) 
  
                print('Average Lukas-Kanade velocity for '+str(velocities_i)+' timestep: ' + avg_velocity_lk_str +'\n')            
        

            else: 
                avg_velocity_lk_str = 'NaN'       
                avg_velocity_lk_km_s_str = 'NaN'       

            line = line + avg_velocity_lk_str +', '
            line_km_s = line_km_s + avg_velocity_lk_km_s_str +', '


            if (self.decompose_config.calc_horn_schunk == 'True'): 
            

                vf_scalar_hs_i = np.copy(self.vf_scalar_hs_norm[velocities_i])
                dest_points_hs_i = np.copy(self.dest_points_hs[velocities_i])
                avg_velocity_hs_i = np.copy(self.avg_velocity_hs[velocities_i])

                mask_k.save_data_FITS(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_hs',  vf_scalar_hs_i, velocities_i, 0, mask_k.header_orig)
                mask_k.save_data_norm(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_hs', vf_scalar_hs_i, velocities_i, 0, 'y')
                mask_k.save_histogram(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_hs_hist', vf_scalar_hs_i, velocities_i, 0)
                
                avg_velocity_hs_str = str(avg_velocity_hs_i) 
                avg_velocity_hs_km_s_str= str(avg_velocity_hs_i*self.decompose_config.vlct_multipl_constant) 
  
                print('Average Horn-Schunk velocity for '+str(velocities_i)+' timestep: ' + avg_velocity_hs_str +'\n')            
                           
            else: 
                avg_velocity_hs_str = 'NaN'
                avg_velocity_hs_km_s_str = 'NaN'        

            line = line + avg_velocity_hs_str +', '    
            line_km_s = line_km_s + avg_velocity_hs_km_s_str +', '

            if (self.decompose_config.calc_farneback == 'True'): 
            

                vf_scalar_fb_i = np.copy(self.vf_scalar_fb_norm[velocities_i])
                dest_points_fb_i = np.copy(self.dest_points_fb[velocities_i])
                avg_velocity_fb_i = np.copy(self.avg_velocity_fb[velocities_i])

                mask_k.save_data_FITS(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_fb',  vf_scalar_fb_i, velocities_i, 0, mask_k.header_orig)
                mask_k.save_data_norm(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_fb', vf_scalar_fb_i, velocities_i, 0, 'y')
                #mask_k.save_histogram(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_fb_hist', vf_scalar_fb_i, velocities_i, 0)
                
                avg_velocity_fb_str = str(avg_velocity_fb_i) 
                avg_velocity_fb_km_s_str= str(avg_velocity_fb_i*self.decompose_config.vlct_multipl_constant) 

                print('Average Farneback velocity for '+str(velocities_i)+' timestep: ' + avg_velocity_fb_str +'\n')            
                           
            else: 
                avg_velocity_fb_str = 'NaN'
                avg_velocity_fb_km_s_str = 'NaN'           

            line = line + avg_velocity_fb_str +', '   
            line_km_s = line_km_s + avg_velocity_fb_km_s_str +', '           



            if (self.decompose_config.calc_flct == 'True'): 
            
                vf_scalar_flct_i = np.copy(self.vf_scalar_flct_norm[velocities_i])
                dest_points_flct_i = np.copy(self.dest_points_flct[velocities_i])
                avg_velocity_flct_i = np.copy(self.avg_velocity_flct[velocities_i])

                mask_k.save_data_FITS(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_flct',  vf_scalar_flct_front_i, velocities_i, 0, mask_k.header_orig)
                mask_k.save_data_norm(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_flct_norm', vf_scalar_flct_front_i, velocities_i, 0, 'y')
                mask_k.save_data(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_flct', vf_scalar_flct_front_i, velocities_i, 0, 'y')
                #mask_k.save_histogram(os.getcwd()+self.decompose_config.velocities_save_path, 'vel_scalar_flct_hist', vf_scalar_flct_front_i, velocities_i, 0)
               
                avg_velocity_flct_str= str(avg_velocity_flct_i) 
                avg_velocity_flct_km_s_str= str(avg_velocity_flct_i*self.decompose_config.vlct_multipl_constant) 

                print('Average FLCT velocity for '+str(velocities_i)+' timestep: ' + avg_velocity_flct_str +'\n')            
               

            else: 
                avg_velocity_flct_str = 'NaN'       
                avg_velocity_flct_km_s_str = 'NaN'

            line = line + avg_velocity_flct_str +', '  
            line_km_s = line_km_s + avg_velocity_flct_km_s_str +', '  


            if (self.decompose_config.calc_mass_cent_vl == 'True'): 
             
               avg_velocity_geom_cent_str = str(self.avg_velocity_geom_cent[velocities_i]) 
               avg_velocity_mass_cent_str = str(self.avg_velocity_mass_cent[velocities_i]) 
               avg_velocity_geom_cent_km_s_str = str(self.avg_velocity_geom_cent[velocities_i]*self.decompose_config.vlct_multipl_constant)  
               avg_velocity_mass_cent_km_s_str = str(self.avg_velocity_mass_cent[velocities_i]*self.decompose_config.vlct_multipl_constant) 
               
               print('Average geom. center velocity for '+str(velocities_i)+' timestep: '+avg_velocity_geom_cent_str +'\n')            
               print('Average mass  center velocity for '+str(velocities_i)+' timestep: '+avg_velocity_mass_cent_str +'\n')            
          

            else: 
            
                avg_velocity_geom_cent_str = 'NaN'
                avg_velocity_mass_cent_str = 'NaN' 
                avg_velocity_geom_cent_km_s_str = 'NaN'
                avg_velocity_mass_cent_km_s_str = 'NaN' 


            
            line = line + avg_velocity_geom_cent_str + ', ' + avg_velocity_mass_cent_str+'\n' 
            line_km_s = line_km_s + avg_velocity_geom_cent_km_s_str + ', ' + avg_velocity_mass_cent_km_s_str+'\n' 
        

            velocities_f.write(line)
            velocities_front_f.write(line_front) 

            velocities_f_km_s.write(line_km_s)
            velocities_front_f_km_s.write(line_front_km_s) 
        


        results_overlap_f.close()
        results_coord_f.close()

        velocities_f.close()
        velocities_front_f.close() 
        
        velocities_f_km_s.close()
        velocities_front_f_km_s.close() 




    def __init__(self): 
        
   
        self.decompose_config = t_config.t_decompose_config()
        self.preproc_config = t_config.t_preproc_config()
        
        self.velocities_n = 0
        self.vf_scalar_lk_front = []
        self.dest_points_lk_front = []
        self.avg_velocity_lk_front = [] 
        self.lk_front_n = 0       
  
        self.vf_scalar_hs_front = []
        self.dest_points_hs_front = []
        self.avg_velocity_hs_front = []
        self.hs_front_n = 0  

        self.vf_scalar_fb_front = []
        self.dest_points_fb_front = []
        self.avg_velocity_fb_front = []
        self.fb_front_n = 0 

        self.vf_scalar_flct_front = [] 
        self.dest_points_flct_front = [] 
        self.avg_velocity_flct_front = [] 
        self.flct_front_n = 0  

        self.avg_mass_cent_vl_front = 0
        self.avg_geom_cent_vl_front = 0

        self.vf_scalar_lk = []
        self.dest_points_lk = []
        self.avg_velocity_lk = []
        self.lk_n = 0
        
        self.vf_scalar_hs = [] 
        self.dest_points_hs = []
        self.avg_velocity_hs = []
        self.hs_n = 0

        self.vf_scalar_fb = [] 
        self.dest_points_fb = []
        self.avg_velocity_fb = []
        self.fb_n = 0 

        self.vf_scalar_flct = []
        self.dest_points_flct = []
        self.avg_velocity_flct = []
        self.flct_n = 0     

        self.avg_velocity_mass_cent_front = []
        self.avg_velocity_geom_cent_front = []
        
        self.avg_velocity_mass_cent = []
        self.avg_velocity_geom_cent = []

        self.avg_mass_cent_vl = 0
        self.avg_geom_cent_vl = 0

        self.vf_scalar_lk_front_max = 0
        self.dest_points_lk_front_max = 0 
        
        self.vf_scalar_hs_front_max = 0
        self.dest_points_hs_front_max = 0
        self.vf_scalar_fb_front_max = 0
        self.dest_points_fb_front_max = 0
        self.vf_scalar_flct_front_max = 0
        self.dest_points_flct_front_max = 0
                    
        self.vf_scalar_lk_max = 0 
        self.dest_points_lk_max = 0
        self.vf_scalar_hs_max = 0
        self.dest_points_hs_max = 0
        self.vf_scalar_fb_max = 0
        self.dest_points_fb_max = 0
        self.vf_scalar_flct_max = 0
        self.dest_points_flct_max = 0
        self.datetime_strs = []


if __name__ == "__main__":
    main()