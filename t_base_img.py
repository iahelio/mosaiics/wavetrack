import glob
import datetime
import sunpy.map
#from sunpy.instr.aia import aiaprep
from matplotlib import cm
import numpy as np
import sunpy.map
from sunpy.image.resample import resample as sunpy_image_resample
from astropy.utils.data import download_file
from astropy.io import fits
from aiapy.calibrate import register
from pprint import pprint
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt1
import matplotlib.colors as clr
import io
import t_math
from skimage import color
from skimage import io
from t_config import t_preproc_config
from t_config import t_decompose_config
import cv2
import astropy.io.fits as iofits
from PIL import Image, ImageOps
import astropy.units as u


class t_base_img(object):
# v.0.41

# Reresents basic I/O and serves amd works a parent for other classes such 
# as t_atrou_wavelet, t_mask, t_aware, and  others 


    # Rebin AIA headers when necessary 
    def rebin_map_header(self,oldmap,new_shape,rebinfac=None):
        newmap=sunpy.map.Map(oldmap.data,oldmap.meta)
        if rebinfac is None: rebinfac=newmap.meta['naxis1']/new_shape[0]
        newmap.meta['cdelt1']=newmap.meta['cdelt1']*rebinfac
        newmap.meta['cdelt2']=newmap.meta['cdelt2']*rebinfac
        newmap.meta['naxis1']=new_shape[0]
        newmap.meta['naxis2']=new_shape[1]
        newmap.meta['crpix1']=new_shape[0]/2.
        newmap.meta['crpix2']=new_shape[1]/2.
        newmap.meta['imscl_mp']=newmap.meta['imscl_mp']*rebinfac
        newmap.meta['x0_mp']=newmap.meta['x0_mp']/rebinfac
        newmap.meta['y0_mp']=newmap.meta['y0_mp']/rebinfac
        return newmap


    def read_input_file(self, img_fname):
        
        a_math = t_math.t_math()

      
        if (self.preproc_config.input_data_format=='FITS'):
          
           print('Reading file:' + img_fname)

           # AIA level 1-> level 1_5 data 
           img_init_lev_1=sunpy.map.Map(img_fname)

           if (self.preproc_config.sunpy_resample=='y'):
               new_dimensions = [self.preproc_config.x_size, self.preproc_config.y_size] * u.pixel
               img_init_lev_1 = img_init_lev_1.resample(new_dimensions)

              # img_init_lev_1=sunpy_image_resample(img_init_lev_1,[self.preproc_config.x_size, self.preproc_config.y_size])
               #1/self.preproc_config.rebin_factor])

           
           self.header_orig = img_init_lev_1.meta
 
           print('Date:' + str(img_init_lev_1.date))         
           self.datetime_str=str(img_init_lev_1.date) 
           #self.datetime_str='NaN'#img_fname[3:22]
           file_datetime_obj=datetime.datetime.strptime(str(img_init_lev_1.date),'%Y-%m-%dT%H:%M:%S.%f' )
           self.timestamp=file_datetime_obj.timestamp()      
           print('Timestamp:'+str(self.timestamp))
           #self.datetime_str=str(img_init_lev_1.date)

           print('Lookup on lev_1->lev_1.5 pre-processing data')

           print('******************************************')
           print('img_init2_lev_1...: ')
           print('type(img_init_lev_1):')
           print(type(img_init_lev_1))
           print('img_init_lev_1.data.shape:')
           print(img_init_lev_1.data.shape)
           print('pprint(img_init_lev_1):')
           pprint(img_init_lev_1)
        
           print('Min:', np.min(img_init_lev_1.data))
           print('Max:', np.max(img_init_lev_1.data))
           print('Mean:', np.mean(img_init_lev_1.data))
           print('Stdev:', np.std(img_init_lev_1.data))
                
           print('img_init_lev_1.scale:')
           print(img_init_lev_1.scale)
           print('img_init_lev_1.rotation_matrix:')
           print(img_init_lev_1.rotation_matrix)
           
           if (self.preproc_config.manual_rebin_patch=='y'):
               img_init_lev_1=self.rebin_map_header(img_init_lev_1,[self.preproc_config.x_size,self.preproc_config.y_size],self.preproc_config.rebin_factor)
     
           
           if (self.preproc_config.use_preprocessed_png=='y'): 

               img_fname_png=img_fname.replace(self.preproc_config.fits_extension,'.png', 1)  
               img_fname_png=img_fname_png.replace('data','data_png', 1) 
               img_arr=np.array(io.imread(img_fname_png,as_gray=True)) 



           elif ( (self.preproc_config.data_lev=='AIA_1') or (self.preproc_config.data_lev=='OTHER')):        
              print('Going forward with AIA level 1 data...')            
              img_arr=np.array(img_init_lev_1.data)
            

           elif (self.preproc_config.data_lev=='AIA_1.5'):
              print('Concerting to AIA level 1.5 data')  
              print('img_init_lev_1_5 = register(img_init_lev_1)...: ')
              img_init_lev_1_5 = register(img_init_lev_1)
              img_arr=np.array(img_init_lev_1_5.data)
      
           elif (self.preproc_config.data_lev=='AIA_1_resized_log'):
              
              print('Using resized level 1 data on log scale with dpi=400 resolution (alpha version, testing purposes only, check image borders !)...')         
              print('Warning: depricated. Does not change headers')
              fig=plt.figure()
              fig = plt.figure(figsize=(10,10), dpi=400)
              plt.axis('off')
              img_init_lev_1_imshow_log=plt.imshow(img_init_lev_1.data, cmap='gray', norm=clr.LogNorm()) 
              exp_resize_data_log=a_math.export_resize_fig_matplotlib(img_init_lev_1_imshow_log, 'img_init_lev_1_imshow_log_'+self.datetime_str+'.png',self.preproc_config.x_size,self.preproc_config.y_size)
              plt.close()
              img_arr=exp_resize_data_log
           
           else: 
              print('Error, Wrong data level specified !')
        
        
        elif (self.preproc_config.input_data_format=='raster'):
            img_arr=np.array(io.imread(img_fname,as_gray=True))        
        else: 
            print('Error: possible input data formats are FITS and raster')


        if (self.preproc_config.resize=='y'):
            print('Resizing...')  
            img_arr = cv2.resize(img_arr, dsize=(self.preproc_config.x_size, self.preproc_config.y_size), interpolation=cv2.INTER_CUBIC)
            new_dimensions = [self.preproc_config.x_size, self.preproc_config.y_size] * u.pixel

            if (self.preproc_config.data_lev=='AIA_1.5'):
                dummy_data=img_init_lev_1_5.resample(new_dimensions)
            else: 
                dummy_data=img_init_lev_1.resample(new_dimensions)

            header_orig_save=self.header_orig
           
            self.header_orig=dummy_data.meta
            self.header_orig['naxis1']= self.preproc_config.x_size
            self.header_orig['naxis2']= self.preproc_config.y_size
           
            #if (self.preproc_config.use_manal_rsun=='y'):
            #    self.header_orig['R_SUN'] = self.preproc_config.manual_rsun
            #else:       
            #    self.header_orig['R_SUN'] = np.uint16(header_orig_save['R_SUN']/self.preproc_config.rebin_factor)
    
            self.header_orig['R_SUN'] = np.uint16(header_orig_save['R_SUN']/self.preproc_config.rebin_factor)
    

        self.img_init_data_orig = list(img_arr)
        self.img_bkg_data = list(img_arr)


        img_arr_low_trunc = (img_arr>self.preproc_config.low_threshold)*img_arr
        img_arr_trunc_orig = (img_arr<self.preproc_config.high_threshold)*img_arr_low_trunc 
     
        
        if (self.preproc_config.adjust_dynrange=='y'):
            
            img_arr_trunc = a_math.bring_to_int32(img_arr_trunc_orig)
            print('Adjusting dynamic range...')
            img_arr_dynrange_no_invert=a_math.adjust_dynrange_data(img_arr_trunc, self.preproc_config.dynrange_logbase) 
            img_arr_dynrange=np.array(img_arr_dynrange_no_invert)
        
        else:
            
            if (self.preproc_config.data_invert_img_load=='y'):
               
               img_arr_trunc_orig_32 = a_math.bring_to_int32(img_arr_trunc_orig)
               print('Inverting data...')
               
               if (self.preproc_config.data_invert_method=='numeric'): 
                   img_arr_dynrange=a_math.invert_data_numeric(img_arr_trunc_orig_32) 
               
               elif (self.preproc_config.data_invert_method=='img'):
                   img_arr_dynrange=a_math.invert_data_img(img_arr_trunc_orig_32)

    
               self.header_orig['DATAMIN']=np.amin(img_arr_dynrange) 
               self.header_orig['DATAMAX']=np.amax(img_arr_dynrange)
      
            else: 
               img_arr_dynrange=np.array(img_arr_trunc_orig)
         
        
        if (self.preproc_config.apply_circle_mask=='n'):
            self.img_init_data = list(img_arr_dynrange)
        
        print('Solar disk position according to metadata:')


        if (self.preproc_config.data_lev!='OTHER'): 
            self.preproc_config.disk_radius=int(self.header_orig['R_SUN']*self.preproc_config.rsun_coeff)
      
        # self.preproc_config.disk_center_x=int(self.header_orig['X0_LFLimb'])
        # self.preproc_config.disk_center_y=int(self.header_orig['Y0_LFLimb'])
      
        self.preproc_config.disk_center_x=np.uint16(self.preproc_config.x_size/2)
        self.preproc_config.disk_center_y=np.uint16(self.preproc_config.y_size/2)
       
        #warning, legacy part with pre-set mask.
        if (self.preproc_config.apply_circle_mask=='y'):
            print('Applying circle mask...')
            img_init_data_arr=np.multiply(img_arr_dynrange,self.circle_mask) 
            self.img_init_data=list(img_init_data_arr)
         
        
    def get_save_fname(self,path,name, data, param_1, param_2):
        
        if (param_1<10):
            zeros_str='00'
        elif ((param_1<100) and (param_1>=10)):
            zeros_str='0'
        elif (param_1>=100):
            zeros_str=''

        return path+'/'+name+'_'+'t_'+zeros_str+str(param_1)+'-'+str(param_2)+'_'+self.datetime_str+'.png'


    
    def save_data(self,path,name, data, param_1, param_2,use_imsave):

        full_fname = self.get_save_fname(path,name, data, param_1, param_2) + '.png'

        print('Saving data:'+full_fname)
        
        data_image=Image.fromarray(np.array(data))
        data_image_flip=ImageOps.flip(data_image)
        data_flip=np.asarray(data_image_flip)

        if (use_imsave=='y'): 
            plt.imsave(arr = data_flip, fname=full_fname)   
        else:
            plt.imsave(arr = data_flip, fname=full_fname,cmap='gray')
        
        return full_fname
    

    '''
    def save_data_norm(self,path,name, data, param_1, param_2,use_imsave):

        full_fname = self.get_save_fname(path,name, data, param_1, param_2) + '.png'
        scalar_field_normalized = (data - np.min(data)) / (np.max(data) - np.min(data)) * 255
        scalar_field_uint8 = scalar_field_normalized.astype(np.uint8)
        plt.imsave(full_fname, scalar_field_uint8, cmap='gray')        
        return full_fname    
    '''
    

    def data_norm(self, data):

        data_norm=(data - np.min(data)) / (np.max(data) - np.min(data)).astype(np.float32)
        return data_norm


    def save_data_norm(self, path, name, data, param_1, param_2, use_imsave):

        full_fname = self.get_save_fname(path,name, data, param_1, param_2) + '.png'
        print('Saving data:'+full_fname)
        
        data_image=Image.fromarray(np.array(data))
        data_image_flip=ImageOps.flip(data_image)
        data_flip=np.asarray(data_image_flip)
        data_norm=(data_flip - np.min(data_flip)) / (np.max(data_flip) - np.min(data_flip)).astype(np.float32)

        if (use_imsave=='y'): 
            plt.imsave(arr = data_norm, fname=full_fname,cmap='jet')   
        else:
            plt.imsave(arr = data_norm, fname=full_fname,cmap='gray')
        
        return full_fname
    
 

    def save_data_norm_bg(self, path, name, data, param_1, param_2, use_imsave, data_background=None):

        full_fname = self.get_save_fname(path, name, data, param_1, param_2) + '.png'
        print('Saving data:' + full_fname)

        data_image = Image.fromarray(data)
        data_image_flip = ImageOps.flip(data_image)
        data_flip = np.asarray(data_image_flip)

        # Normalize data
        data_norm = (data_flip - np.min(data_flip)) / (np.max(data_flip) - np.min(data_flip))

        # If a background data is provided, normalize and blend it with data_norm
        if data_background is not None:
            data_background_image = Image.fromarray(data_background)
            data_background_flip = ImageOps.flip(data_background_image)
            data_background_norm = (data_background_flip - np.min(data_background_flip)) / (np.max(data_background_flip) - np.min(data_background_flip))

            # Blend data_norm and data_background_norm
            # Assuming an average blend (0.5 weight for each). Adjust weights if needed.
            data_norm = 0.5 * data_norm + 0.5 * data_background_norm

        # Save the image
        if (use_imsave == 'y'):
            plt.imsave(arr=data_norm, fname=full_fname, cmap='jet')
        else:
            plt.imsave(arr=data_norm, fname=full_fname, cmap='gray')

        return full_fname


    def save_histogram(self, path, name, velocity_field, param_1, param_2, bins=100):
       
        full_fname = self.get_save_fname(path, name, velocity_field, param_1, param_2) + '.png'
        print('Saving histogram:' + full_fname)
            
        # Flatten the velocity field for histogram computation
        flat_velocity = velocity_field.flatten()

        # Filter out non-finite values (inf or NaN)
        finite_velocities = flat_velocity[np.isfinite(flat_velocity)]

        # Create the histogram plot
        plt.figure(figsize=(10, 6))
        plt.hist(finite_velocities, bins=bins, color='c', edgecolor='black', alpha=0.7)
        
        # Set title and labels
        plt.title("Histogram of Velocity Field Scalar Values")
        plt.xlabel("Velocity")
        plt.ylabel("Number of Pixels")
        plt.grid(True, which='both', linestyle='--', linewidth=0.5)
        plt.tight_layout()

        # Save the plot to a .png file
        plt.savefig(full_fname, dpi=300)
        plt.close()



    def save_data_FITS(self,path,name, data, param_1, param_2,header):

        if (param_1<10):
            zeros_str='00'
        elif ((param_1<100) and (param_1>=10)):
            zeros_str='0'
        elif (param_1>=100):
            zeros_str=''

        #full_fname_fits= path+'/'+name+'_'+'t_'+zeros_str+str(param_1)+'-'+str(param_2)+'_'+self.datetime_str+'.fits'
        full_fname_fits = self.get_save_fname(path,name, data, param_1, param_2) + '.fits'

        print('Saving FITS file:')
        print(full_fname_fits)
        
        astropy_header = fits.Header()

        # Transfer header information
        for key, value in header.items():
            try:
               astropy_header[key] = value
            except: 
               print('Warning, wrong astropy header value:'+str(value))


        hdu = fits.PrimaryHDU(data, header=astropy_header)
        hdu.writeto(full_fname_fits, overwrite=True)
        
        return full_fname_fits


    


    def __init__(self):

        self.preproc_config = t_preproc_config()
        self.decompose_config = t_decompose_config()
        
        self.preproc_config_bkg = t_preproc_config()
        self.decompose_config_bkg = t_decompose_config()

        self.preproc_config_bkg.low_threshold_bd=-100
        self.preproc_config_bkg.high_threshold_bd=40000
        self.preproc_config_bkg.use_base_diff ='n'
 
        self.decompose_config_bkg.scales_n_max = 5
        self.decompose_config_bkg.scales_weights=[0,0.5,0.5,1,1,1,0,0,0,0]
        self.decompose_config.apply_thrhold_circle_mask = 'y'
        self.decompose_config.thrhold_sigma= 0.5
        self.decompose_config.grad_thrhold_sigma= 0
        self.decompose_config.outside_thrhold_sigma=0.5
        

        '''
        self.circle_mask = [] 
        self.square_mask = []
        '''
        
        self.timestamp = int()
        self.datetime_str = str()
        self.wavelength = 0 
        self.timestep_i = 0
    
      