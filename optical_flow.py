import numpy as np

'''
    



    2) Horn-Schunck Method:
    The Horn-Schunck algorithm is an iterative method that estimates optical flow by assuming that the motion field is smooth and the intensity changes are consistent across the entire image. The main steps involved in the Horn-Schunck method are as follows:

    Define the smoothness constraint: Assume that the flow field is smooth and can be represented by a spatial gradient.

    Formulate an energy functional: Construct an energy function that incorporates both data term and smoothness term. The data term measures the difference in intensities between the two frames, while the smoothness term encourages smooth flow fields.

    Minimize the energy functional: Optimize the energy function by iteratively solving a set of partial differential equations using numerical techniques like Gauss-Seidel relaxation or successive over-relaxation.

    Iterative Update: In each iteration, update the flow field by taking into account the neighboring flow values and the local intensity gradients.



    The Horn-Schunck algorithm provides dense optical flow estimation, meaning it assigns a flow vector to each pixel. However, it assumes global smoothness and may produce inaccurate results in the presence of large displacements or occlusions.
'''




def lucas_kanade(prev_frame, curr_frame, features, window_size=15):
    # Define the window for local neighborhood
    half_size = window_size // 2
    
    # Create arrays to store the optical flow vectors, velocity scalar, and velocity vector fields
    optical_flow = np.zeros_like(features, dtype=np.float32)
    velocity_field_scalar = np.zeros_like(prev_frame, dtype=np.float32)
    velocity_field_vector = np.zeros_like(prev_frame, dtype=np.float32)
    
    for i, (x, y) in enumerate(features):
        # Extract the window around the feature point in the previous frame
        prev_window = prev_frame[y-half_size:y+half_size+1, x-half_size:x+half_size+1]
        
        # Compute the gradients in the x and y directions for the window
        gradient_x = np.gradient(prev_window, axis=1)
        gradient_y = np.gradient(prev_window, axis=0)
        
        # Compute the temporal gradient between the frames
        temporal_gradient = curr_frame - prev_frame
        
        # Compute the optical flow vector using least squares
        A = np.column_stack((gradient_x.flatten(), gradient_y.flatten()))
        b = -temporal_gradient[y-half_size:y+half_size+1, x-half_size:x+half_size+1].flatten()
        flow_vector, _, _, _ = np.linalg.lstsq(A, b, rcond=None)
        
        # Assign the optical flow vector to the corresponding feature point
        optical_flow[i] = flow_vector
        
        # Compute the magnitude of the optical flow vector as the velocity scalar
        velocity_scalar = np.linalg.norm(flow_vector)
        
        # Compute the destination point using the optical flow vector as the velocity vector
        destination_point = (x + flow_vector[0], y + flow_vector[1])
        
        # Assign the velocity scalar and velocity vector to the corresponding pixels in the fields
        velocity_field_scalar[int(y), int(x)] = velocity_scalar
        velocity_field_vector[int(y), int(x)] = destination_point
    
    # Compute the average velocity scalar
    average_velocity = np.mean(velocity_field_scalar)
    
    return optical_flow, velocity_field_scalar, velocity_field_vector, average_velocity
